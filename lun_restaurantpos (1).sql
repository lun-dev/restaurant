-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 10, 2022 at 08:42 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lun_restaurantpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `key`, `value`) VALUES
(7, 'date_format', 'l jS F Y (H:i:s)'),
(8, 'language', 'en'),
(17, 'is_human_date_format', '1'),
(18, 'app_name', 'Smart Delivery'),
(19, 'app_short_description', 'Manage Mobile Application'),
(20, 'mail_driver', 'smtp'),
(21, 'mail_host', 'smtp.hostinger.com'),
(22, 'mail_port', '587'),
(23, 'mail_username', 'productdelivery@smartersvision.com'),
(24, 'mail_password', 'NnvAwk&&E7'),
(25, 'mail_encryption', 'ssl'),
(26, 'mail_from_address', 'productdelivery@smartersvision.com'),
(27, 'mail_from_name', 'Smarter Vision'),
(30, 'timezone', 'America/Montserrat'),
(32, 'theme_contrast', 'light'),
(33, 'theme_color', 'primary'),
(34, 'app_logo', '020a2dd4-4277-425a-b450-426663f52633'),
(35, 'nav_color', 'navbar-light bg-white'),
(38, 'logo_bg_color', 'bg-white'),
(66, 'default_role', 'admin'),
(68, 'facebook_app_id', '518416208939727'),
(69, 'facebook_app_secret', '93649810f78fa9ca0d48972fee2a75cd'),
(71, 'twitter_app_id', 'twitter'),
(72, 'twitter_app_secret', 'twitter 1'),
(74, 'google_app_id', '527129559488-roolg8aq110p8r1q952fqa9tm06gbloe.apps.googleusercontent.com'),
(75, 'google_app_secret', 'FpIi8SLgc69ZWodk-xHaOrxn'),
(77, 'enable_google', '1'),
(78, 'enable_facebook', '1'),
(93, 'enable_stripe', '1'),
(94, 'stripe_key', 'pk_test_pltzOnX3zsUZMoTTTVUL4O41'),
(95, 'stripe_secret', 'sk_test_o98VZx3RKDUytaokX4My3a20'),
(101, 'custom_field_models.0', 'App\\Models\\User'),
(104, 'default_tax', '10'),
(107, 'default_currency', '$'),
(108, 'fixed_header', '0'),
(109, 'fixed_footer', '0'),
(110, 'fcm_key', 'AAAAHMZiAQA:APA91bEb71b5sN5jl-w_mmt6vLfgGY5-_CQFxMQsVEfcwO3FAh4-mk1dM6siZwwR3Ls9U0pRDpm96WN1AmrMHQ906GxljILqgU2ZB6Y1TjiLyAiIUETpu7pQFyicER8KLvM9JUiXcfWK'),
(111, 'enable_notifications', '1'),
(112, 'paypal_username', 'sb-z3gdq482047_api1.business.example.com'),
(113, 'paypal_password', 'JV2A7G4SEMLMZ565'),
(114, 'paypal_secret', 'AbMmSXVaig1ExpY3utVS3dcAjx7nAHH0utrZsUN6LYwPgo7wfMzrV5WZ'),
(115, 'enable_paypal', '1'),
(116, 'main_color', '#25D366'),
(117, 'main_dark_color', '#25D366'),
(118, 'second_color', '#043832'),
(119, 'second_dark_color', '#ccccdd'),
(120, 'accent_color', '#8c98a8'),
(121, 'accent_dark_color', '#9999aa'),
(122, 'scaffold_dark_color', '#2c2c2c'),
(123, 'scaffold_color', '#fafafa'),
(124, 'google_maps_key', 'AIzaSyAT07iMlfZ9bJt1gmGj9KhJDLFY8srI6dA'),
(125, 'mobile_language', 'en'),
(126, 'app_version', '2.0.0'),
(127, 'enable_version', '1'),
(128, 'default_currency_id', '1'),
(129, 'default_currency_code', 'USD'),
(130, 'default_currency_decimal_digits', '2'),
(131, 'default_currency_rounding', '0'),
(132, 'currency_right', '0'),
(133, 'home_section_1', 'search'),
(134, 'home_section_2', 'slider'),
(135, 'home_section_3', 'top_markets_heading'),
(136, 'home_section_4', 'top_markets'),
(137, 'home_section_5', 'trending_week_heading'),
(138, 'home_section_6', 'trending_week'),
(139, 'home_section_7', 'categories_heading'),
(140, 'home_section_8', 'categories'),
(141, 'home_section_9', 'popular_heading'),
(142, 'home_section_10', 'popular'),
(143, 'home_section_11', 'recent_reviews_heading'),
(144, 'home_section_12', 'recent_reviews'),
(145, 'home_section_1', 'search'),
(146, 'home_section_2', 'slider'),
(147, 'home_section_3', 'top_markets_heading'),
(148, 'home_section_4', 'top_markets'),
(149, 'home_section_5', 'trending_week_heading'),
(150, 'home_section_6', 'trending_week'),
(151, 'home_section_7', 'categories_heading'),
(152, 'home_section_8', 'categories'),
(153, 'home_section_9', 'popular_heading'),
(154, 'home_section_10', 'popular'),
(155, 'home_section_11', 'recent_reviews_heading'),
(156, 'home_section_12', 'recent_reviews');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_options`
--

CREATE TABLE `cart_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Vegetables', 'Numquam incidunt tenetur ea repudiandae. Ab nisi suscipit suscipit et esse dolorum vel. Explicabo nemo hic neque at fugit consequatur. Sed ea aperiam aliquid odio tempora reiciendis. Minima asperiores nesciunt qui quos voluptatum.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(2, 'Fish', 'Officia ut possimus minus omnis modi. Similique accusantium voluptas rem laudantium commodi. Praesentium ex quae cupiditate maiores praesentium temporibus assumenda. Illum ad culpa qui impedit sunt autem sit. Rerum nisi quam qui harum rerum.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(3, 'Medicines', 'Et corporis quod quia enim possimus provident quisquam. Ex blanditiis atque consequatur quo modi quasi vel excepturi. Sunt est consequatur rem itaque unde laboriosam ut. Dolor dolorum et accusantium illum est dignissimos reprehenderit. Architecto odio perspiciatis libero a in.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(4, 'Drinks', 'Quia dolorem occaecati in ut fuga voluptatem. Aliquam doloremque nemo corporis commodi qui corporis. Eos vero tenetur voluptatibus atque. Ut non a est. Et quos ipsam sed numquam ut facilis.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(5, 'Fruit', 'Ducimus nemo iusto voluptas nihil nostrum sit. Accusamus unde et non non consequatur dolorum fugit. Eius non recusandae ex ad labore sit. Inventore perspiciatis magni assumenda animi. Officiis praesentium eos odit.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(6, 'Fruit', 'Delectus dignissimos illo quae. Reprehenderit maiores quibusdam ut voluptates omnis corporis. Quam sed exercitationem rerum quo voluptas id. Optio accusamus consequatur cupiditate necessitatibus. Dolor necessitatibus mollitia doloribus incidunt porro dolores dolores.', '2022-01-04 10:36:16', '2022-01-04 10:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'percent',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(63) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_digits` tinyint(3) UNSIGNED DEFAULT NULL,
  `rounding` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `code`, `decimal_digits`, `rounding`, `created_at`, `updated_at`) VALUES
(1, 'US Dollar', '$', 'USD', 2, 0, '2019-10-22 13:50:48', '2019-10-22 13:50:48'),
(2, 'Euro', '€', 'EUR', 2, 0, '2019-10-22 13:51:39', '2019-10-22 13:51:39'),
(3, 'Indian Rupee', 'টকা', 'INR', 2, 0, '2019-10-22 13:52:50', '2019-10-22 13:52:50'),
(4, 'Indonesian Rupiah', 'Rp', 'IDR', 0, 0, '2019-10-22 13:53:22', '2019-10-22 13:53:22'),
(5, 'Brazilian Real', 'R$', 'BRL', 2, 0, '2019-10-22 13:54:00', '2019-10-22 13:54:00'),
(6, 'Cambodian Riel', '៛', 'KHR', 2, 0, '2019-10-22 13:55:51', '2019-10-22 13:55:51'),
(7, 'Vietnamese Dong', '₫', 'VND', 0, 0, '2019-10-22 13:56:26', '2019-10-22 13:56:26');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `in_table` tinyint(1) DEFAULT NULL,
  `bootstrap_column` tinyint(4) DEFAULT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `custom_field_model` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `name`, `type`, `values`, `disabled`, `required`, `in_table`, `bootstrap_column`, `order`, `custom_field_model`, `role`, `created_at`, `updated_at`) VALUES
(8, 'Market_name', 'text', NULL, 0, 1, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:09:33', '2022-01-04 11:09:33'),
(10, 'National_ID', 'text', NULL, 0, 1, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:11:37', '2022-01-04 11:11:37'),
(13, 'Fees_for_service', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:15:56', '2022-01-04 11:15:56'),
(14, 'Number_of_branches', 'number', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:16:25', '2022-01-04 11:16:25'),
(17, 'Bank_account', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:19:45', '2022-01-04 11:19:45'),
(24, 'Description', 'textarea', NULL, 0, 1, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-08 06:34:17', '2022-01-08 06:34:17'),
(28, 'Address', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'market', '2022-01-08 09:47:46', '2022-01-08 09:47:46'),
(29, 'Latitude', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'market', '2022-01-08 09:48:11', '2022-01-08 09:48:11'),
(30, 'Longitude', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'market', '2022-01-08 09:48:35', '2022-01-08 09:48:35'),
(31, 'Delivery_fees', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'market', '2022-01-08 09:49:02', '2022-01-08 09:49:02'),
(32, 'Delivery_range', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'market', '2022-01-08 09:49:28', '2022-01-08 09:49:28'),
(33, 'Active', 'select', '[\"yes\",\"no\"]', 0, 0, 0, 6, NULL, 'App\\Models\\User', 'market', '2022-01-08 09:49:56', '2022-01-08 09:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_values`
--

CREATE TABLE `custom_field_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_id` int(10) UNSIGNED NOT NULL,
  `customizable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customizable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_field_values`
--

INSERT INTO `custom_field_values` (`id`, `value`, `view`, `custom_field_id`, `customizable_type`, `customizable_id`, `created_at`, `updated_at`) VALUES
(164, '121', '121', 8, 'App\\Models\\User', 25, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(165, '1122336688', '1122336688', 10, 'App\\Models\\User', 25, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(166, NULL, NULL, 13, 'App\\Models\\User', 25, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(167, '2', '2', 14, 'App\\Models\\User', 25, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(168, NULL, NULL, 17, 'App\\Models\\User', 25, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(172, '<p>dsfdaf</p>', 'dsfdaf', 24, 'App\\Models\\User', 25, '2022-01-08 07:18:43', '2022-01-08 07:18:43'),
(173, '121', '121', 8, 'App\\Models\\User', 49, '2022-01-09 09:55:53', '2022-01-09 09:55:53'),
(174, '8808774410', '8808774410', 10, 'App\\Models\\User', 49, '2022-01-09 09:55:53', '2022-01-09 09:55:53'),
(175, '30', '30', 13, 'App\\Models\\User', 49, '2022-01-09 09:55:53', '2022-01-09 09:55:53'),
(176, '1', '1', 14, 'App\\Models\\User', 49, '2022-01-09 09:55:53', '2022-01-09 09:55:53'),
(177, '0', '0', 17, 'App\\Models\\User', 49, '2022-01-09 09:55:53', '2022-01-09 09:55:53'),
(178, '<p>fdsgdsghdfshf</p>', 'fdsgdsghdfshf', 24, 'App\\Models\\User', 49, '2022-01-09 09:55:53', '2022-01-09 09:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `days`
--

INSERT INTO `days` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Saturday'),
(2, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Sunday'),
(3, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Monday'),
(4, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Tuesday'),
(5, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Wednesday'),
(6, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Thursday'),
(7, '2022-01-08 09:57:14', '2022-01-08 09:57:14', 'Friday');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_addresses`
--

CREATE TABLE `delivery_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_addresses`
--

INSERT INTO `delivery_addresses` (`id`, `description`, `address`, `latitude`, `longitude`, `is_default`, `user_id`, `created_at`, `updated_at`) VALUES
(5, 'Culpa fugit deserunt eos exercitationem minima eveniet et quia.', '86411 Viva Lake Suite 277\nWest Clifford, MO 72314', '41.109192', '-70.85215', 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 'Ducimus sed sed beatae iure voluptas.', '54358 Willa Plain Suite 635\nBinstown, MO 75676', '-12.635885', '-5.709121', 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'Fugit dolores harum eaque sunt harum esse corporis.', '202 Yasmeen Ferry\nGerlachville, IN 42447-4066', '-25.89909', '-78.121823', 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'Necessitatibus est illo a mollitia temporibus facilis beatae.', '800 Jamal Ramp\nRowebury, IN 85374-4669', '-11.474496', '-59.584587', 0, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `discountables`
--

CREATE TABLE `discountables` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `discountable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discountable_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `delivery_fee` double(5,2) NOT NULL DEFAULT 0.00,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `earning` double(9,2) NOT NULL DEFAULT 0.00,
  `available` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers_payouts`
--

CREATE TABLE `drivers_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `driver_markets`
--

CREATE TABLE `driver_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `earnings`
--

CREATE TABLE `earnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `total_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `admin_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `market_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `delivery_fee` double(9,2) NOT NULL DEFAULT 0.00,
  `tax` double(9,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `faq_category_id`, `created_at`, `updated_at`) VALUES
(1, 'Facilis velit nesciunt esse aperiam. Ut porro mollitia eius dolorum dolorum aliquam nihil.', 'She\'ll get me executed, as sure as ferrets are ferrets! Where CAN I have ordered\'; and she dropped it hastily, just in time to wash the things between whiles.\' \'Then you should say what you mean,\'.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 'Alias neque voluptatum consectetur aut est. Rem rerum commodi fugit nihil velit doloremque est aut.', 'I shall think nothing of tumbling down stairs! How brave they\'ll all think me at all.\' \'In that case,\' said the Cat, and vanished. Alice was not a moment to think that there was nothing so VERY.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 'Earum fugiat ut veniam. Autem harum ut perspiciatis et nam. Ex iste amet illo dignissimos et et.', 'March Hare. Visit either you like: they\'re both mad.\' \'But I don\'t know what they\'re like.\' \'I believe so,\' Alice replied eagerly, for she felt certain it must be what he did it,) he did not like.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 'Sint suscipit quos ea. Suscipit tenetur dolorum laudantium illo qui facilis.', 'I got up and said, \'That\'s right, Five! Always lay the blame on others!\' \'YOU\'D better not do that again!\' which produced another dead silence. \'It\'s a pun!\' the King hastily said, and went on: \'But.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 'Ut dolorum at et ut atque dicta accusantium. Cumque dolorem deserunt possimus.', 'Alice heard the Queen ordering off her head!\' Alice glanced rather anxiously at the righthand bit again, and did not come the same as they were gardeners, or soldiers, or courtiers, or three of the.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 'Omnis qui et enim nesciunt qui. Cumque maxime ratione at aut. Nemo ut est ea iste qui.', 'Alice thought she had forgotten the words.\' So they had been all the right words,\' said poor Alice, who was reading the list of the crowd below, and there was no more to be a Caucus-race.\' \'What IS.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, 'Enim qui laudantium suscipit repellat assumenda quibusdam. Quod inventore eos et quis quisquam.', 'I used--and I don\'t take this young lady tells us a story.\' \'I\'m afraid I can\'t understand it myself to begin with; and being ordered about by mice and rabbits. I almost think I should understand.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 'Non aperiam quae illo voluptatum aliquam pariatur facilis dicta. Voluptas rerum odit hic quia.', 'Alice in a low, timid voice, \'If you didn\'t like cats.\' \'Not like cats!\' cried the Mock Turtle, suddenly dropping his voice; and the words a little, \'From the Queen. \'Their heads are gone, if it had.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 'Repellat sit dolorem molestias omnis voluptate repellendus aut. Dolor a impedit hic in.', 'Shark, But, when the White Rabbit read:-- \'They told me he was speaking, so that by the way, was the Cat went on, half to Alice. \'Nothing,\' said Alice. \'Exactly so,\' said Alice. \'Why not?\' said the.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 'Cupiditate esse et mollitia doloribus. Quia voluptates aliquid tenetur error quas similique.', 'Dormouse shall!\' they both cried. \'Wake up, Alice dear!\' said her sister; \'Why, what are YOUR shoes done with?\' said the Cat, and vanished. Alice was very fond of pretending to be two people! Why.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'Voluptates adipisci illo eligendi tempora voluptatem a sunt. Dignissimos ut magni veniam odit.', 'Caterpillar. This was not going to dive in among the trees under which she had wept when she had but to her to speak again. The rabbit-hole went straight on like a wild beast, screamed \'Off with his.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 'Saepe et aliquid earum et nesciunt doloremque. Ipsa autem ut quis repellendus.', 'I shall have to fly; and the other two were using it as far as they would die. \'The trial cannot proceed,\' said the Dormouse go on with the grin, which remained some time with one eye; but to her.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, 'Qui labore aut voluptatem ab esse. Illo dolores dicta autem et nihil.', 'NOT, being made entirely of cardboard.) \'All right, so far,\' thought Alice, \'they\'re sure to happen,\' she said this, she was not a bit of stick, and held it out again, and all the children she knew.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, 'Labore sequi amet quia iure. Nesciunt qui accusantium ut omnis. Hic eum numquam est.', 'Alice soon came to the shore, and then sat upon it.) \'I\'m glad they\'ve begun asking riddles.--I believe I can reach the key; and if it had gone. \'Well! I\'ve often seen them so often, you know.\' \'Not.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'Officia et cum et blanditiis omnis veritatis ad. Animi in laudantium iure aut quod est soluta.', 'At this moment Alice felt a little wider. \'Come, it\'s pleased so far,\' thought Alice, \'it\'ll never do to hold it. As soon as the Caterpillar decidedly, and he hurried off. Alice thought she had to.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(16, 'Pariatur rerum tempore deserunt nihil et reprehenderit. Est molestiae qui at quam saepe atque est.', 'I do so like that curious song about the crumbs,\' said the Hatter: \'but you could keep it to his ear. Alice considered a little, half expecting to see that the Queen had ordered. They very soon had.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(17, 'Ea fuga non repudiandae magnam maiores accusamus. Molestiae unde repudiandae aut voluptatem et.', 'Soup, so rich and green, Waiting in a hurried nervous manner, smiling at everything about her, to pass away the time. Alice had never heard it before,\' said Alice,) and round the hall, but they all.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(18, 'Magnam alias repudiandae quos ea nam ad dolores. Aliquam esse nihil minima eum.', 'ME, and told me he was obliged to have been a RED rose-tree, and we put a white one in by mistake; and if it thought that SOMEBODY ought to have wondered at this, but at the moment, \'My dear! I.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(19, 'Et non expedita quam. Ut necessitatibus sequi dolorem commodi.', 'Lory, as soon as she tucked it away under her arm, with its mouth again, and put it in with the clock. For instance, if you want to see it again, but it makes rather a complaining tone, \'and they.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(20, 'Officia fugit vel qui tempora aut dolorem. Dignissimos veritatis illum vel est magnam minima.', 'Caterpillar. Alice thought decidedly uncivil. \'But perhaps he can\'t help it,\' said the Gryphon: and it said in a deep, hollow tone: \'sit down, both of you, and must know better\'; and this was his.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(21, 'Repellendus quia quas ratione repudiandae quo sit. Cum quis fugit animi quisquam aut distinctio.', 'There was exactly three inches high). \'But I\'m not Ada,\' she said, without even waiting to put it into his cup of tea, and looked very uncomfortable. The moment Alice appeared, she was dozing off.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(22, 'Explicabo est sint aut iusto ipsam eaque. Quia ratione voluptatem non itaque voluptatem saepe fuga.', 'Time as well she might, what a wonderful dream it had lost something; and she felt sure she would manage it. \'They must go back by railway,\' she said to the Knave \'Turn them over!\' The Knave did so.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(23, 'Nostrum dolore non veniam quisquam. Magni dolores sit molestiae temporibus voluptas sint sit.', 'I THINK,\' said Alice. \'Who\'s making personal remarks now?\' the Hatter went on all the first to break the silence. \'What day of the house if it please your Majesty,\' said the Duck. \'Found IT,\' the.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(24, 'Sunt iusto atque nesciunt molestiae officiis. Facere provident rerum quam maiores eaque.', 'YOU sing,\' said the Duck. \'Found IT,\' the Mouse was bristling all over, and both the hedgehogs were out of the evening, beautiful Soup! Soup of the game, feeling very glad that it made no mark; but.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(25, 'Qui cum alias voluptatem aut ipsam ipsum cum quia. Quia possimus tempora et rerum ut velit.', 'Alice, who felt very lonely and low-spirited. In a minute or two, and the words a little, \'From the Queen. \'You make me smaller, I can guess that,\' she added in an impatient tone: \'explanations take.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(26, 'Delectus et consectetur sed cumque sint quia. Et iusto a pariatur officiis alias aut facere.', 'Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, but in a tone of great relief. \'Call the first witness,\' said the Mock Turtle in a very long silence, broken only by.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(27, 'Accusantium minus dicta rerum sint. Quo maiores eaque a nisi quo et et.', 'Alice in a whisper.) \'That would be a walrus or hippopotamus, but then she had not the smallest idea how confusing it is all the arches are gone from this morning,\' said Alice sadly. \'Hand it over a.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(28, 'Hic libero illum dolores ab vitae animi. Nihil fugit ea tempore totam. Enim ex sunt quas nam.', 'Seaography: then Drawling--the Drawling-master was an uncomfortably sharp chin. However, she got into a chrysalis--you will some day, you know--and then after that savage Queen: so she waited. The.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(29, 'Eos impedit a corrupti. Nam esse ipsum maiores non eius repudiandae.', 'It was all ridges and furrows; the balls were live hedgehogs, the mallets live flamingoes, and the soldiers shouted in reply. \'That\'s right!\' shouted the Queen, who was beginning to write out a.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(30, 'Incidunt qui repellat voluptas soluta numquam. Ut quis suscipit sunt aut dolorem.', 'Hatter, \'when the Queen shrieked out. \'Behead that Dormouse! Turn that Dormouse out of its mouth again, and put it to make herself useful, and looking at it uneasily, shaking it every now and then.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Products', '2019-08-31 10:31:52', '2019-08-31 10:31:52'),
(2, 'Services', '2019-08-31 10:32:03', '2019-08-31 10:32:03'),
(3, 'Delivery', '2019-08-31 10:32:11', '2019-08-31 10:32:11'),
(4, 'Misc', '2019-08-31 10:32:17', '2019-08-31 10:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorite_options`
--

CREATE TABLE `favorite_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `favorite_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Grocery', 'Eum similique maiores atque quia explicabo. Dolores quia placeat consequatur id quis perspiciatis. Ducimus sit ducimus officia labore maiores et porro. Est iusto natus nesciunt debitis consequuntur totam. Et illo et autem inventore earum corrupti.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(2, 'Pharmacy', 'Eaque et aut natus. Minima blanditiis ut sunt distinctio ad. Quasi doloremque rerum ex rerum. Molestias similique similique aut rerum delectus blanditiis et. Dolorem et quas nostrum est nobis.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(3, 'Restaurant', 'Est nihil omnis natus ducimus ducimus excepturi quos. Et praesentium in quia veniam. Tempore aut nesciunt consequatur pariatur recusandae. Voluptatem commodi eius quaerat est deleniti impedit. Qui quo harum est sequi incidunt labore eligendi cum.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(4, 'Store', 'Ex nostrum suscipit aut et labore. Ut dolor ut eum eum voluptatem ex. Sapiente in tempora soluta voluptatem. Officia accusantium quae sit. Rerum esse ipsa molestias dolorem et est autem consequatur.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(5, 'Electronics', 'Dolorum earum ut blanditiis blanditiis. Facere quis voluptates assumenda saepe. Ab aspernatur voluptatibus rem doloremque cum impedit. Itaque blanditiis commodi repudiandae asperiores. Modi atque placeat consectetur et aut blanditiis.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(6, 'Furniture', 'Est et iste enim. Quam repudiandae commodi rerum non esse. Et in aut sequi est aspernatur. Facere non modi expedita asperiores. Ipsa laborum saepe deserunt qui consequatur voluptas inventore dolorum.', '2020-04-11 13:03:21', '2020-04-11 13:03:21');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `markets`
--

CREATE TABLE `markets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_commission` double(8,2) DEFAULT 0.00,
  `delivery_fee` double(8,2) DEFAULT 0.00,
  `delivery_range` double(8,2) DEFAULT 0.00,
  `default_tax` double(8,2) DEFAULT 0.00,
  `closed` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 0,
  `available_for_delivery` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `markets`
--

INSERT INTO `markets` (`id`, `name`, `description`, `address`, `latitude`, `longitude`, `phone`, `mobile`, `information`, `admin_commission`, `delivery_fee`, `delivery_range`, `default_tax`, `closed`, `active`, `available_for_delivery`, `created_at`, `updated_at`) VALUES
(13, 'name', NULL, 'sdhfdshf', '11.23337', '11.23330', '01201298111', '01203298102', NULL, 0.00, 20.00, 30.00, 0.00, 0, 1, 1, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(15, 'dsgsg', NULL, 'dfshfhfd', '11.23337', '11.23330', '01203371117', '01203298102', NULL, 0.00, 20.00, 30.00, 0.00, 0, 1, 1, '2022-01-09 07:37:22', '2022-01-09 07:37:22'),
(16, 'dsgsg', NULL, 'dfshfhfd', '11.23337', '11.23330', '01203371117', '01203298102', NULL, 0.00, 20.00, 30.00, 0.00, 0, 1, 1, '2022-01-09 07:38:48', '2022-01-09 09:11:12'),
(17, 'helldafgs', NULL, 'gvasgsd', '', '', '01277298111', '01203298102', NULL, 0.00, 0.00, 0.00, 0.00, 0, 1, 1, '2022-01-09 10:00:44', '2022-01-09 10:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `markets_payouts`
--

CREATE TABLE `markets_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `market_days`
--

CREATE TABLE `market_days` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED DEFAULT NULL,
  `day_id` int(10) UNSIGNED NOT NULL,
  `start_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cloase_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `market_days`
--

INSERT INTO `market_days` (`id`, `market_id`, `day_id`, `start_from`, `cloase_at`, `created_at`, `updated_at`) VALUES
(1, 13, 1, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(2, 13, 2, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(3, 13, 3, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(4, 13, 4, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(5, 13, 5, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(6, 13, 6, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(7, 13, 7, NULL, NULL, '2022-01-09 07:31:24', '2022-01-09 07:31:24'),
(8, 16, 1, '13:04', '17:04', '2022-01-09 07:38:48', '2022-01-09 09:10:46'),
(9, 16, 2, '14:05', '19:05', '2022-01-09 07:38:48', '2022-01-09 09:11:12'),
(10, 16, 3, NULL, NULL, '2022-01-09 07:38:48', '2022-01-09 07:38:48'),
(11, 16, 4, NULL, NULL, '2022-01-09 07:38:48', '2022-01-09 07:38:48'),
(12, 16, 5, NULL, NULL, '2022-01-09 07:38:48', '2022-01-09 07:38:48'),
(13, 16, 6, NULL, NULL, '2022-01-09 07:38:48', '2022-01-09 07:38:48'),
(14, 16, 7, NULL, NULL, '2022-01-09 07:38:48', '2022-01-09 07:38:48'),
(15, 17, 1, '14:00', '18:00', '2022-01-09 10:00:44', '2022-01-09 10:00:44'),
(16, 17, 2, '14:00', '18:00', '2022-01-09 10:00:44', '2022-01-09 10:00:44'),
(17, 17, 3, '14:00', '18:00', '2022-01-09 10:00:44', '2022-01-09 10:00:44'),
(18, 17, 4, NULL, NULL, '2022-01-09 10:00:44', '2022-01-09 10:00:44'),
(19, 17, 5, NULL, NULL, '2022-01-09 10:00:44', '2022-01-09 10:00:44'),
(20, 17, 6, NULL, NULL, '2022-01-09 10:00:44', '2022-01-09 10:00:44'),
(21, 17, 7, NULL, NULL, '2022-01-09 10:00:44', '2022-01-09 10:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `market_fields`
--

CREATE TABLE `market_fields` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `market_reviews`
--

CREATE TABLE `market_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(76, 'App\\Models\\Upload', 23, 'logo', 'Screenshot from 2021-12-11 12-38-29', 'Screenshot-from-2021-12-11-12-38-29.png', 'image/png', 'public', 128457, '[]', '{\"uuid\":\"866c2517-1168-4c19-a767-c1aeb6eea0ab\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 1, '2022-01-08 05:58:31', '2022-01-08 05:58:31'),
(77, 'App\\Models\\Upload', 24, 'legal_attachment', 'Screenshot from 2021-12-25 13-43-10', 'Screenshot-from-2021-12-25-13-43-10.png', 'image/png', 'public', 153212, '[]', '{\"uuid\":\"bcc496a6-f8e3-4b7d-92ca-0941bbbd4322\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 2, '2022-01-08 05:58:35', '2022-01-08 05:58:35'),
(78, 'App\\Models\\Upload', 25, 'legal_attachment', 'Screenshot from 2022-01-04 13-54-44', 'Screenshot-from-2022-01-04-13-54-44.png', 'image/png', 'public', 54661, '[]', '{\"uuid\":\"b4979702-a41c-45f9-addc-b860bf994bee\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 3, '2022-01-08 05:58:37', '2022-01-08 05:58:37'),
(79, 'App\\Models\\User', 25, 'logo', 'Screenshot from 2021-12-11 12-38-29', 'Screenshot-from-2021-12-11-12-38-29.png', 'image/png', 'public', 128457, '[]', '{\"uuid\":\"866c2517-1168-4c19-a767-c1aeb6eea0ab\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 4, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(80, 'App\\Models\\User', 25, 'legal_attachment', 'Screenshot from 2022-01-04 13-54-44', 'Screenshot-from-2022-01-04-13-54-44.png', 'image/png', 'public', 54661, '[]', '{\"uuid\":\"b4979702-a41c-45f9-addc-b860bf994bee\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 5, '2022-01-08 05:58:44', '2022-01-08 05:58:44'),
(81, 'App\\Models\\Upload', 26, 'logo', 'Screenshot from 2021-12-25 13-43-10', 'Screenshot-from-2021-12-25-13-43-10.png', 'image/png', 'public', 153212, '[]', '{\"uuid\":\"d622d437-edfa-43c0-95c1-52d752922c3d\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 6, '2022-01-09 07:17:51', '2022-01-09 07:17:52'),
(82, 'App\\Models\\Upload', 27, 'legal_attachment', 'Screenshot from 2022-01-04 13-54-44', 'Screenshot-from-2022-01-04-13-54-44.png', 'image/png', 'public', 54661, '[]', '{\"uuid\":\"e5b8f6b5-0012-4ea7-83aa-0ac6b7ea1725\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 7, '2022-01-09 07:17:55', '2022-01-09 07:17:56'),
(83, 'App\\Models\\Upload', 28, 'logo', 'Screenshot from 2021-12-25 13-43-10', 'Screenshot-from-2021-12-25-13-43-10.png', 'image/png', 'public', 153212, '[]', '{\"uuid\":\"d48234c9-f5f5-488b-b033-be6c3843c0e0\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 8, '2022-01-09 09:25:28', '2022-01-09 09:25:28'),
(84, 'App\\Models\\Upload', 29, 'image', 'Screenshot from 2021-12-11 12-38-29', 'Screenshot-from-2021-12-11-12-38-29.png', 'image/png', 'public', 128457, '[]', '{\"uuid\":\"06fe7ff6-5108-4abe-8050-f16ac41c4bca\",\"user_id\":50,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 9, '2022-01-09 10:46:56', '2022-01-09 10:46:57'),
(85, 'App\\Models\\Upload', 30, 'image', 'Screenshot from 2022-01-04 13-54-44', 'Screenshot-from-2022-01-04-13-54-44.png', 'image/png', 'public', 54661, '[]', '{\"uuid\":\"39b0452b-0a4d-4595-a812-b5ec8a5aa6d8\",\"user_id\":50,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 10, '2022-01-09 10:56:27', '2022-01-09 10:56:27'),
(86, 'App\\Models\\Product', 42, 'image', 'Screenshot from 2022-01-04 13-54-44', 'Screenshot-from-2022-01-04-13-54-44.png', 'image/png', 'public', 54661, '[]', '{\"uuid\":\"39b0452b-0a4d-4595-a812-b5ec8a5aa6d8\",\"user_id\":50,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}', '[]', 11, '2022-01-09 10:56:46', '2022-01-09 10:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_26_175145_create_permission_tables', 1),
(4, '2018_06_12_140344_create_media_table', 1),
(5, '2018_06_13_035117_create_uploads_table', 1),
(6, '2018_07_17_180731_create_settings_table', 1),
(7, '2018_07_24_211308_create_custom_fields_table', 1),
(8, '2018_07_24_211327_create_custom_field_values_table', 1),
(9, '2019_08_29_213820_create_fields_table', 1),
(10, '2019_08_29_213821_create_markets_table', 1),
(11, '2019_08_29_213822_create_categories_table', 1),
(12, '2019_08_29_213826_create_option_groups_table', 1),
(13, '2019_08_29_213829_create_faq_categories_table', 1),
(14, '2019_08_29_213833_create_order_statuses_table', 1),
(15, '2019_08_29_213837_create_products_table', 1),
(16, '2019_08_29_213838_create_options_table', 1),
(17, '2019_08_29_213842_create_galleries_table', 1),
(18, '2019_08_29_213847_create_product_reviews_table', 1),
(19, '2019_08_29_213921_create_payments_table', 1),
(20, '2019_08_29_213922_create_delivery_addresses_table', 1),
(21, '2019_08_29_213926_create_faqs_table', 1),
(22, '2019_08_29_213940_create_market_reviews_table', 1),
(23, '2019_08_30_152927_create_favorites_table', 1),
(24, '2019_08_31_111104_create_orders_table', 1),
(25, '2019_09_04_153857_create_carts_table', 1),
(26, '2019_09_04_153858_create_favorite_options_table', 1),
(27, '2019_09_04_153859_create_cart_options_table', 1),
(28, '2019_09_04_153958_create_product_orders_table', 1),
(29, '2019_09_04_154957_create_product_order_options_table', 1),
(30, '2019_09_04_163857_create_user_markets_table', 1),
(31, '2019_10_22_144652_create_currencies_table', 1),
(32, '2019_12_14_134302_create_driver_markets_table', 1),
(33, '2020_03_25_094752_create_drivers_table', 1),
(34, '2020_03_25_094802_create_earnings_table', 1),
(35, '2020_03_25_094809_create_drivers_payouts_table', 1),
(36, '2020_03_25_094817_create_markets_payouts_table', 1),
(37, '2020_03_27_094855_create_notifications_table', 1),
(38, '2020_04_11_135804_create_market_fields_table', 1),
(39, '2020_08_23_181022_create_coupons_table', 1),
(40, '2020_08_23_181029_create_discountables_table', 1),
(41, '2020_09_01_192732_create_slides_table', 1),
(42, '2022_01_08_070611_add_phone_table_users', 2),
(43, '2021_12_04_082607_create_days_table', 3),
(44, '2022_01_08_124459_add_parnet_id_table_users', 4),
(45, '2021_12_04_082607_create_market_days_table', 5),
(46, '2022_01_09_123801_add__calories_table_products', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 2),
(3, 'App\\Models\\User', 9),
(3, 'App\\Models\\User', 10),
(3, 'App\\Models\\User', 11),
(3, 'App\\Models\\User', 12),
(3, 'App\\Models\\User', 13),
(3, 'App\\Models\\User', 14),
(3, 'App\\Models\\User', 15),
(3, 'App\\Models\\User', 16),
(3, 'App\\Models\\User', 17),
(3, 'App\\Models\\User', 18),
(3, 'App\\Models\\User', 19),
(3, 'App\\Models\\User', 20),
(3, 'App\\Models\\User', 21),
(3, 'App\\Models\\User', 22),
(3, 'App\\Models\\User', 23),
(3, 'App\\Models\\User', 24),
(3, 'App\\Models\\User', 25),
(3, 'App\\Models\\User', 35),
(3, 'App\\Models\\User', 37),
(3, 'App\\Models\\User', 38),
(3, 'App\\Models\\User', 43),
(3, 'App\\Models\\User', 44),
(3, 'App\\Models\\User', 45),
(3, 'App\\Models\\User', 46),
(3, 'App\\Models\\User', 47),
(3, 'App\\Models\\User', 48),
(3, 'App\\Models\\User', 49),
(4, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 4),
(4, 'App\\Models\\User', 26),
(4, 'App\\Models\\User', 27),
(4, 'App\\Models\\User', 28),
(4, 'App\\Models\\User', 29),
(4, 'App\\Models\\User', 30),
(4, 'App\\Models\\User', 32),
(4, 'App\\Models\\User', 33),
(4, 'App\\Models\\User', 34),
(4, 'App\\Models\\User', 39),
(4, 'App\\Models\\User', 40),
(4, 'App\\Models\\User', 41),
(4, 'App\\Models\\User', 42),
(4, 'App\\Models\\User', 50),
(5, 'App\\Models\\User', 5),
(5, 'App\\Models\\User', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `option_groups`
--

CREATE TABLE `option_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_groups`
--

INSERT INTO `option_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Size', '2019-08-31 08:55:28', '2019-08-31 08:55:28'),
(2, 'Color', '2019-10-09 11:26:28', '2019-10-09 11:26:28'),
(3, 'Parfum', '2019-10-09 11:26:28', '2019-10-09 11:26:28'),
(4, 'Taste', '2019-10-09 11:26:28', '2019-10-09 11:26:28');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `tax` double(5,2) DEFAULT 0.00,
  `delivery_fee` double(5,2) DEFAULT 0.00,
  `hint` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_address_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Order Received', '2019-08-30 14:39:28', '2019-10-15 16:03:14'),
(2, 'Preparing', '2019-10-15 16:03:50', '2019-10-15 16:03:50'),
(3, 'Ready', '2019-10-15 16:04:30', '2019-10-15 16:04:30'),
(4, 'On the Way', '2019-10-15 16:04:13', '2019-10-15 16:04:13'),
(5, 'Delivered', '2019-10-15 16:04:30', '2019-10-15 16:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'users.profile', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(2, 'dashboard', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(3, 'medias.create', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(4, 'medias.delete', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(5, 'medias', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(6, 'permissions.index', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(7, 'permissions.edit', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(8, 'permissions.update', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(9, 'permissions.destroy', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(10, 'roles.index', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(11, 'roles.edit', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(12, 'roles.update', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(13, 'roles.destroy', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(14, 'customFields.index', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(15, 'customFields.create', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(16, 'customFields.store', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(17, 'customFields.show', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(18, 'customFields.edit', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(19, 'customFields.update', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(20, 'customFields.destroy', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(21, 'users.login-as-user', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(22, 'users.index', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(23, 'users.create', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(24, 'users.store', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(25, 'users.show', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(26, 'users.edit', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(27, 'users.update', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(28, 'users.destroy', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(29, 'app-settings', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(30, 'markets.index', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(31, 'markets.create', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(32, 'markets.store', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(33, 'markets.edit', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(34, 'markets.update', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(35, 'markets.destroy', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(36, 'categories.index', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(37, 'categories.create', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(38, 'categories.store', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(39, 'categories.edit', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(40, 'categories.update', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(41, 'categories.destroy', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(42, 'faqCategories.index', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(43, 'faqCategories.create', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(44, 'faqCategories.store', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(45, 'faqCategories.edit', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(46, 'faqCategories.update', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(47, 'faqCategories.destroy', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(48, 'orderStatuses.index', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(49, 'orderStatuses.show', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(50, 'orderStatuses.edit', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(51, 'orderStatuses.update', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(52, 'products.index', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(53, 'products.create', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(54, 'products.store', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(55, 'products.edit', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(56, 'products.update', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(57, 'products.destroy', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(58, 'galleries.index', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(59, 'galleries.create', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(60, 'galleries.store', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(61, 'galleries.edit', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(62, 'galleries.update', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(63, 'galleries.destroy', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(64, 'productReviews.index', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(65, 'productReviews.create', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(66, 'productReviews.store', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(67, 'productReviews.edit', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(68, 'productReviews.update', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(69, 'productReviews.destroy', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(76, 'options.index', 'web', '2020-03-29 12:58:09', '2020-03-29 12:58:09', NULL),
(77, 'options.create', 'web', '2020-03-29 12:58:09', '2020-03-29 12:58:09', NULL),
(78, 'options.store', 'web', '2020-03-29 12:58:09', '2020-03-29 12:58:09', NULL),
(79, 'options.show', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(80, 'options.edit', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(81, 'options.update', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(82, 'options.destroy', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(83, 'payments.index', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(84, 'payments.show', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(85, 'payments.update', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(86, 'faqs.index', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(87, 'faqs.create', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(88, 'faqs.store', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(89, 'faqs.edit', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(90, 'faqs.update', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(91, 'faqs.destroy', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(92, 'marketReviews.index', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(93, 'marketReviews.create', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(94, 'marketReviews.store', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(95, 'marketReviews.edit', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(96, 'marketReviews.update', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(97, 'marketReviews.destroy', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(98, 'favorites.index', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(99, 'favorites.create', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(100, 'favorites.store', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(101, 'favorites.edit', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(102, 'favorites.update', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(103, 'favorites.destroy', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(104, 'orders.index', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(105, 'orders.create', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(106, 'orders.store', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(107, 'orders.show', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(108, 'orders.edit', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(109, 'orders.update', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(110, 'orders.destroy', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(111, 'notifications.index', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(112, 'notifications.show', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(113, 'notifications.destroy', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(114, 'carts.index', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(115, 'carts.edit', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(116, 'carts.update', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(117, 'carts.destroy', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(118, 'currencies.index', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(119, 'currencies.create', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(120, 'currencies.store', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(121, 'currencies.edit', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(122, 'currencies.update', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(123, 'currencies.destroy', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(124, 'deliveryAddresses.index', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(125, 'deliveryAddresses.create', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(126, 'deliveryAddresses.store', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(127, 'deliveryAddresses.edit', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(128, 'deliveryAddresses.update', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(129, 'deliveryAddresses.destroy', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(130, 'drivers.index', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(131, 'drivers.create', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(132, 'drivers.store', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(133, 'drivers.show', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(134, 'drivers.edit', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(135, 'drivers.update', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(136, 'drivers.destroy', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(137, 'earnings.index', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(138, 'earnings.create', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(139, 'earnings.store', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(140, 'earnings.show', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(141, 'earnings.edit', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(142, 'earnings.update', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(143, 'earnings.destroy', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(144, 'driversPayouts.index', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(145, 'driversPayouts.create', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(146, 'driversPayouts.store', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(147, 'driversPayouts.show', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(148, 'driversPayouts.edit', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(149, 'driversPayouts.update', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(150, 'driversPayouts.destroy', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(151, 'marketsPayouts.index', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(152, 'marketsPayouts.create', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(153, 'marketsPayouts.store', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(154, 'marketsPayouts.show', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(155, 'marketsPayouts.edit', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(156, 'marketsPayouts.update', 'web', '2020-03-29 12:58:19', '2020-03-29 12:58:19', NULL),
(157, 'marketsPayouts.destroy', 'web', '2020-03-29 12:58:19', '2020-03-29 12:58:19', NULL),
(158, 'permissions.create', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(159, 'permissions.store', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(160, 'permissions.show', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(161, 'roles.create', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(162, 'roles.store', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(163, 'roles.show', 'web', '2020-03-29 12:59:16', '2020-03-29 12:59:16', NULL),
(164, 'fields.index', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(165, 'fields.create', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(166, 'fields.store', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(167, 'fields.edit', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(168, 'fields.update', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(169, 'fields.destroy', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(170, 'optionGroups.index', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(171, 'optionGroups.create', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(172, 'optionGroups.store', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(173, 'optionGroups.edit', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(174, 'optionGroups.update', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(175, 'optionGroups.destroy', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(176, 'requestedMarkets.index', 'web', '2020-08-13 12:58:02', '2020-08-13 12:58:02', NULL),
(183, 'coupons.index', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(184, 'coupons.create', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(185, 'coupons.store', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(186, 'coupons.edit', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(187, 'coupons.update', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(188, 'coupons.destroy', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(189, 'slides.index', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(190, 'slides.create', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(191, 'slides.store', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(192, 'slides.edit', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(193, 'slides.update', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(194, 'slides.destroy', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_price` double(8,2) DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `capacity` double(9,2) DEFAULT 0.00,
  `package_items_count` double(9,2) DEFAULT 0.00,
  `unit` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `featured` tinyint(1) DEFAULT 0,
  `deliverable` tinyint(1) DEFAULT 1,
  `market_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Calories` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `discount_price`, `description`, `capacity`, `package_items_count`, `unit`, `featured`, `deliverable`, `market_id`, `category_id`, `created_at`, `updated_at`, `Calories`, `Weight`) VALUES
(42, 'sfgsfhds', 10.00, 2.00, '<p>fdsgfsdagfd</p>', 0.00, 10.00, '100', 0, 1, 17, 1, '2022-01-09 10:56:46', '2022-01-09 10:56:46', '10', '20');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_order_options`
--

CREATE TABLE `product_order_options` (
  `product_order_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `default` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'superadmin', 'web', 0, '2018-07-21 14:37:56', '2019-09-07 20:42:01', NULL),
(3, 'admin', 'web', 0, '2019-09-07 20:41:38', '2019-09-07 20:41:38', NULL),
(4, 'market', 'web', 1, '2019-09-07 20:41:54', '2019-09-07 20:41:54', NULL),
(5, 'branches', 'web', 0, '2019-12-15 16:50:21', '2019-12-15 16:50:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(2, 3),
(2, 4),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(5, 2),
(5, 3),
(5, 4),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(22, 3),
(23, 2),
(23, 3),
(24, 2),
(24, 3),
(25, 2),
(25, 3),
(26, 2),
(26, 3),
(27, 2),
(27, 3),
(27, 4),
(27, 5),
(28, 2),
(28, 3),
(29, 2),
(30, 2),
(30, 3),
(30, 4),
(30, 5),
(31, 2),
(31, 3),
(31, 4),
(32, 2),
(32, 3),
(32, 4),
(33, 2),
(33, 3),
(34, 2),
(34, 3),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(42, 3),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(48, 3),
(48, 5),
(50, 2),
(51, 2),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(53, 2),
(53, 3),
(53, 4),
(54, 2),
(54, 3),
(54, 4),
(55, 2),
(55, 3),
(55, 4),
(56, 2),
(56, 3),
(56, 4),
(57, 2),
(57, 3),
(57, 4),
(58, 2),
(58, 3),
(59, 2),
(59, 3),
(60, 2),
(60, 3),
(61, 2),
(61, 3),
(62, 2),
(62, 3),
(63, 2),
(63, 3),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(69, 2),
(76, 2),
(76, 3),
(77, 2),
(77, 3),
(78, 2),
(78, 3),
(80, 2),
(80, 3),
(81, 2),
(81, 3),
(82, 2),
(82, 3),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(84, 2),
(85, 2),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(92, 3),
(92, 4),
(92, 5),
(95, 2),
(95, 3),
(95, 4),
(95, 5),
(96, 2),
(96, 3),
(96, 4),
(96, 5),
(97, 2),
(98, 2),
(98, 3),
(98, 4),
(98, 5),
(103, 2),
(103, 3),
(103, 4),
(103, 5),
(104, 2),
(104, 3),
(104, 4),
(104, 5),
(107, 2),
(107, 3),
(107, 4),
(107, 5),
(108, 2),
(108, 3),
(109, 2),
(109, 3),
(110, 2),
(110, 3),
(111, 2),
(111, 3),
(111, 4),
(111, 5),
(112, 2),
(113, 2),
(113, 3),
(113, 4),
(113, 5),
(114, 2),
(114, 3),
(114, 4),
(114, 5),
(117, 2),
(117, 3),
(117, 4),
(117, 5),
(118, 2),
(119, 2),
(120, 2),
(121, 2),
(122, 2),
(123, 2),
(124, 2),
(127, 2),
(128, 2),
(129, 2),
(130, 2),
(130, 3),
(130, 5),
(131, 2),
(134, 2),
(134, 3),
(135, 2),
(135, 3),
(137, 2),
(137, 3),
(138, 2),
(144, 2),
(144, 5),
(145, 2),
(145, 3),
(145, 5),
(146, 2),
(146, 3),
(146, 5),
(148, 2),
(149, 2),
(151, 2),
(151, 3),
(152, 2),
(152, 3),
(153, 2),
(153, 3),
(155, 2),
(156, 2),
(158, 2),
(159, 2),
(160, 2),
(161, 2),
(162, 2),
(163, 2),
(164, 2),
(164, 3),
(164, 4),
(164, 5),
(165, 2),
(166, 2),
(167, 2),
(168, 2),
(169, 2),
(170, 2),
(170, 3),
(171, 2),
(171, 3),
(172, 2),
(172, 3),
(173, 2),
(174, 2),
(175, 2),
(176, 2),
(176, 3),
(176, 4),
(176, 5),
(183, 2),
(183, 3),
(183, 4),
(183, 5),
(184, 2),
(185, 2),
(186, 2),
(186, 3),
(187, 2),
(187, 3),
(188, 2),
(189, 2),
(190, 2),
(191, 2),
(192, 2),
(193, 2),
(194, 2);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED DEFAULT 0,
  `text` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'start',
  `text_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `indicator_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_fit` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'cover',
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `market_id` int(10) UNSIGNED DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `order`, `text`, `button`, `text_position`, `text_color`, `button_color`, `background_color`, `indicator_color`, `image_fit`, `product_id`, `market_id`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 1, 'Harum est eveniet ut.', 'Get Discount', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', NULL, NULL, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 1, 'Tenetur corporis eaque totam voluptatum.', 'Discover It', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', NULL, NULL, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 5, 'Fugiat sit quod tempore doloremque.', 'Discover It', 'end', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', NULL, NULL, 0, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 1, 'Aliquam quo perferendis dicta.', 'Discover It', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', NULL, NULL, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 5, 'Fugiat quos facilis sapiente fuga.', 'Get Discount', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', NULL, NULL, 0, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `uuid`, `created_at`, `updated_at`) VALUES
(23, '866c2517-1168-4c19-a767-c1aeb6eea0ab', '2022-01-08 05:58:31', '2022-01-08 05:58:31'),
(24, 'bcc496a6-f8e3-4b7d-92ca-0941bbbd4322', '2022-01-08 05:58:35', '2022-01-08 05:58:35'),
(25, 'b4979702-a41c-45f9-addc-b860bf994bee', '2022-01-08 05:58:37', '2022-01-08 05:58:37'),
(26, 'd622d437-edfa-43c0-95c1-52d752922c3d', '2022-01-09 07:17:51', '2022-01-09 07:17:51'),
(27, 'e5b8f6b5-0012-4ea7-83aa-0ac6b7ea1725', '2022-01-09 07:17:55', '2022-01-09 07:17:55'),
(28, 'd48234c9-f5f5-488b-b033-be6c3843c0e0', '2022-01-09 09:25:28', '2022-01-09 09:25:28'),
(29, '06fe7ff6-5108-4abe-8050-f16ac41c4bca', '2022-01-09 10:46:56', '2022-01-09 10:46:56'),
(30, '39b0452b-0a4d-4595-a812-b5ec8a5aa6d8', '2022-01-09 10:56:27', '2022-01-09 10:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `braintree_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `device_token`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `braintree_id`, `paypal_email`, `remember_token`, `created_at`, `updated_at`, `phone`, `parent_id`) VALUES
(1, 'Michael E. Quinn', 'admin@demo.com', '$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu', 'PivvPlsQWxPl1bB5KrbKNBuraJit0PrUZekQUgtLyTRuyBq921atFtoR1HuA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'R7fs98kPN1GqFKIt4LrXf41X0TFfrJE92g5ximkBbXCPx4AyMj0w195RtM94', '2018-08-06 20:58:41', '2019-09-27 05:49:45', NULL, NULL),
(35, 'enas', 'asgsdagdsagh', '$2y$10$GIZe1MiEWd.gdSJRXzfdJeTnzBbtWhHJVp9rBV7JtoIoZTrJGqQCa', 'SOWM0zEYhVkYIkar6eJ8u5m1SU1lY0GaICkS0DfLbyNReSMmEqffqqD0cmf5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 07:18:22', '2022-01-09 07:18:22', '01203298102', NULL),
(37, 'enas', 'admin@mail.com', '$2y$10$TVqI4ALZUg2vc4lGmwapaOuX5pYyAksi3zNndsgVrN/VuRJCEdKD.', 'wxBxz67iK790xSpGQZ3pVTG757nNljdcW5x8D8pTHG7nm4SUrMMTuj5imxkE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 07:24:19', '2022-01-09 07:24:19', '01213298102', NULL),
(38, 'hello', 'hello@mail.com', '$2y$10$TnfDnWmVTdsR21mxaKSdP.g6aWpNwzLakMOH9spde0hYJv5suoAny', 'XE4gW6hmvo6VpGB2z87FViST9dWAyUPAgt0FPbmPpZ4NwvpzvQ12urANS6bm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 07:25:59', '2022-01-09 07:25:59', '01203298111', NULL),
(42, 'dsgsg', 'sgfsjnhgsrhtysy@mail.com', '$2y$10$kCZWFO7ifWBNdlxqXCsH3uXuMmOyjXNPO9LvDR48CGbFSOH8Y4/sK', 'pMTvYi9VFZRwPvHcfgrGs69DofZ2VAjfU70FgK9iJm5SiAsXio0zJZU2X7x2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 07:38:48', '2022-01-09 09:11:12', '01203371117', 38),
(43, 'new_admin', 'new_admin@demo.com', '$2y$10$nXKeLxQjy85dNG.ZEs1iXOZp/Qc2q2Z74iEB17xv3xtBZX2NFaEI6', 'MbxEFOn5s1fR7KyuIRgE7Gg35m0wazPmbZY1U5l3GyNjQGkIDyOR1Pec7Jm0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:25:55', '2022-01-09 09:25:55', '01203399100', NULL),
(44, 'new_admin', 'sdagdsag@demo.com', '$2y$10$IJCBA.g2XOWYpF4FEIgMXuU1wnhr6xVopSgCQjt0t2jjZC01abU.S', 'kiAQFuTOhkcoJgg1kHIqmhL92xO4QZQZyCIWP04Qua8OOJsEQbBLAnMz0tOH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:27:47', '2022-01-09 09:27:47', '01200399100', NULL),
(45, 'new_admin', 'api@demo.com', '$2y$10$55VL0AKboabaHR4dBuX6OujS/XnMGvbPC46GAf7WmXvdDaBpzw2Li', 'GUWkeRMWSku2IBWGXOnkHQXyrR4fo8RKL6hFW1QgSdD26P0nfH0Ci22pXYKP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:38:06', '2022-01-09 09:38:06', '01206399100', NULL),
(46, 'new_admin', 'jkghjg@demo.com', '$2y$10$IoAQiVAhswwLRPz19YiCNOvmlBAJSyiezKSJZwOzKeoh2ghtihJDm', '8sHA8jlZbY6qggWLeW7BSAJQEA7IieUfc2wYmkYDf7Lt4ob5o7rCyq8F5c4i', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:40:15', '2022-01-09 09:40:15', '01216399100', NULL),
(47, 'testtest', 'testtest@demo.com', '$2y$10$K3dla52/6G6ESRcHkKCynemu9B8pEPZGtduY7qcBCwoIWKrjHoofy', '45FFcsn1Uy7x2H23BGWu3MaYFIkg4k8q272xq7YvRD8ndBbXQXker7eduJzH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:50:40', '2022-01-09 09:50:40', '01203298174', NULL),
(48, 'dhfjkash', 'dhfjkash@demo.com', '$2y$10$dmBfFxFJdUiekxC6OPuZhuBNWuYKCLOPO/yHypZzx4lISe49TazQ2', 'WG5Jb7n7LeFWXF9GtgN43g9ha6CWMYLSEHIgJ4vnRVcpq7NtUsSDrVYp6IvX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:52:34', '2022-01-09 09:52:34', '01203399000', NULL),
(49, 'dhfjkash', 'asdfasdgfsd@demo.com', '$2y$10$PB4V2g0hl3SUE9mS8nqFG.GFRJ2tzp832BRkWQr4diLleLxemx0JK', 'RsB36OlJ3CghaSRDHNqjtsLgF1q91IMfZIdTkbXuzCZFLcdOVQ6mDy2p1pvV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 09:55:53', '2022-01-09 09:55:53', '01103399000', NULL),
(50, 'helldafgs', 'helldafgs@mail.com', '$2y$10$HU.97pO8xbSIFoMVxJMcsuFJRYhEhllkUGeODM6sexvCcHLGl869u', 'oUNj667PUJ4hspcUHq3XHaoF8NzxfrIP3cqoZ2bY0KNpmgjWLWgkUPu4haTh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-09 10:00:44', '2022-01-09 10:00:44', '01277298111', 37);

-- --------------------------------------------------------

--
-- Table structure for table `user_markets`
--

CREATE TABLE `user_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_markets`
--

INSERT INTO `user_markets` (`user_id`, `market_id`) VALUES
(42, 16),
(50, 17);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_settings_key_index` (`key`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `cart_options`
--
ALTER TABLE `cart_options`
  ADD PRIMARY KEY (`option_id`,`cart_id`),
  ADD KEY `cart_options_cart_id_foreign` (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_field_values_custom_field_id_foreign` (`custom_field_id`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `discountables`
--
ALTER TABLE `discountables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discountables_coupon_id_foreign` (`coupon_id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_user_id_foreign` (`user_id`);

--
-- Indexes for table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_payouts_user_id_foreign` (`user_id`);

--
-- Indexes for table `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `driver_markets_market_id_foreign` (`market_id`);

--
-- Indexes for table `earnings`
--
ALTER TABLE `earnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `earnings_market_id_foreign` (`market_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faqs_faq_category_id_foreign` (`faq_category_id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_product_id_foreign` (`product_id`),
  ADD KEY `favorites_user_id_foreign` (`user_id`);

--
-- Indexes for table `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD PRIMARY KEY (`option_id`,`favorite_id`),
  ADD KEY `favorite_options_favorite_id_foreign` (`favorite_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_market_id_foreign` (`market_id`);

--
-- Indexes for table `markets`
--
ALTER TABLE `markets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markets_payouts_market_id_foreign` (`market_id`);

--
-- Indexes for table `market_days`
--
ALTER TABLE `market_days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `market_days_market_id_foreign` (`market_id`),
  ADD KEY `market_days_day_id_foreign` (`day_id`);

--
-- Indexes for table `market_fields`
--
ALTER TABLE `market_fields`
  ADD PRIMARY KEY (`field_id`,`market_id`),
  ADD KEY `market_fields_market_id_foreign` (`market_id`);

--
-- Indexes for table `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `market_reviews_user_id_foreign` (`user_id`),
  ADD KEY `market_reviews_market_id_foreign` (`market_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_product_id_foreign` (`product_id`),
  ADD KEY `options_option_group_id_foreign` (`option_group_id`);

--
-- Indexes for table `option_groups`
--
ALTER TABLE `option_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_order_status_id_foreign` (`order_status_id`),
  ADD KEY `orders_driver_id_foreign` (`driver_id`),
  ADD KEY `orders_delivery_address_id_foreign` (`delivery_address_id`),
  ADD KEY `orders_payment_id_foreign` (`payment_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_market_id_foreign` (`market_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_orders_product_id_foreign` (`product_id`),
  ADD KEY `product_orders_order_id_foreign` (`order_id`);

--
-- Indexes for table `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD PRIMARY KEY (`product_order_id`,`option_id`),
  ADD KEY `product_order_options_option_id_foreign` (`option_id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slides_product_id_foreign` (`product_id`),
  ADD KEY `slides_market_id_foreign` (`market_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `user_markets`
--
ALTER TABLE `user_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `user_markets_market_id_foreign` (`market_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `discountables`
--
ALTER TABLE `discountables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `earnings`
--
ALTER TABLE `earnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `markets`
--
ALTER TABLE `markets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `market_days`
--
ALTER TABLE `market_days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `market_reviews`
--
ALTER TABLE `market_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `option_groups`
--
ALTER TABLE `option_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cart_options`
--
ALTER TABLE `cart_options`
  ADD CONSTRAINT `cart_options_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD CONSTRAINT `custom_field_values_custom_field_id_foreign` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD CONSTRAINT `delivery_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discountables`
--
ALTER TABLE `discountables`
  ADD CONSTRAINT `discountables_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `drivers`
--
ALTER TABLE `drivers`
  ADD CONSTRAINT `drivers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD CONSTRAINT `drivers_payouts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD CONSTRAINT `driver_markets_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `driver_markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `earnings`
--
ALTER TABLE `earnings`
  ADD CONSTRAINT `earnings_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faqs`
--
ALTER TABLE `faqs`
  ADD CONSTRAINT `faqs_faq_category_id_foreign` FOREIGN KEY (`faq_category_id`) REFERENCES `faq_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD CONSTRAINT `favorite_options_favorite_id_foreign` FOREIGN KEY (`favorite_id`) REFERENCES `favorites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorite_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD CONSTRAINT `markets_payouts_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `market_days`
--
ALTER TABLE `market_days`
  ADD CONSTRAINT `market_days_day_id_foreign` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_days_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `market_fields`
--
ALTER TABLE `market_fields`
  ADD CONSTRAINT `market_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_fields_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD CONSTRAINT `market_reviews_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_option_group_id_foreign` FOREIGN KEY (`option_group_id`) REFERENCES `option_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_delivery_address_id_foreign` FOREIGN KEY (`delivery_address_id`) REFERENCES `delivery_addresses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `product_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_orders_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD CONSTRAINT `product_order_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_order_options_product_order_id_foreign` FOREIGN KEY (`product_order_id`) REFERENCES `product_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `slides_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `user_markets`
--
ALTER TABLE `user_markets`
  ADD CONSTRAINT `user_markets_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
/**
 * File name: CartsOfUsersCriteria.php
 * Last modified: 2020.04.30 at 08:24:08
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Criteria\Cart;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CartsOfUsersCriteria.
 *
 * @package namespace App\Criteria\Products;
 */
class CartsOfUsersCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private $user_id;

    /**
     * CartsOfUsersCriteria constructor.
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('user_id', '=', $this->user_id);
    }
}

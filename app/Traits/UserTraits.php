<?php


namespace App\Traits;

use App\Models\Driver;
use App\Models\User;
use App\Models\Market;
use App\Models\MarketDay;
use Illuminate\Support\Facades\DB;

trait UserTraits
{
    public function add_user_admin_role($input)
    {
        $input['roles'] = isset($input['roles']) ? $input['roles'] : 'market';
        $input['api_token'] = str_random(60);
        $user = $this->userRepository->create($input);
        $user->syncRoles($input['roles']);
        $market = Market::create([
            'name' => $input['name'],
            'description' => $user['description'],
            'address' => $input['address']  ?? '',
            'latitude' => $input['latitude'] ?? '',
            'longitude' => $input['longitude'] ?? '',
            'phone' => $user->phone,
            'mobile' => $input['mobile'] ?? '',
            'delivery_fee' => $input['delivery_fee'] ?? '',
            'delivery_range' => $input['delivery_range'] ?? '',
            'active' => 1,
            $input['cover'] != '' ? $input['cover']->move('markets/' . $user->id, $input['cover']->getClientOriginalName()) : '',
        ]);
        DB::table('user_markets')->insert([
            'user_id' => $user->id,
            'market_id' => $market->id,
        ]);
        foreach($input['day_id'] as $key => $val){
            $list = new MarketDay();
            $list->day_id = $val;
            $list->start_from = $input['start_from'][$key];
            $list->cloase_at = $input['cloase_at'][$key];
            $list->market_id = $market->id;
            $list->save();
        }
    }

    public function getUserMarket($id)
    {
        return Market::whereHas('users',function ($q) use($id){
            $q->where('user_id',$id);
        })->with('day')->first();
    }

    public function edit_user_admin_role($input,$id)
    {
        $user = User::findOrFail($id);
        $market = Market::whereHas('users',function ($q) use($id){
            $q->where('user_id',$id);
        })->with('day')->first();
        $updated = Market::where('id',$market->id)->update([
            'name' => $input['name'],
            'description' => $user['description'],
            'address' => $input['address']  ?? '',
            'latitude' => $input['latitude'] ?? '',
            'longitude' => $input['longitude'] ?? '',
            'phone' => $user->phone,
            'mobile' => $input['mobile'] ?? '',
            'delivery_fee' => $input['delivery_fee'] ?? '',
            'delivery_range' => $input['delivery_range'] ?? '',
            'active' => 1,
        ]);
        foreach($input['day_id'] as $k => $i){
            $day = MarketDay::findOrFail($i);
            $day->start_from = $input['start_from'][$k];
            $day->cloase_at = $input['cloase_at'][$k];
            $day->save();
        }
    }

    public function add_user_market_role($input)
    {
        $input['roles'] = isset($input['roles']) ? $input['roles'] : 'driver';
        $input['api_token'] = str_random(60);
        $user = $this->userRepository->create($input);
        $user->syncRoles($input['roles']);
        Driver::create([
            'user_id' => $user->id,
            'birth_of_date' => $input['birth_of_date'],
            'nationalID' => $input['nationalID'],

            'driver_back_license' => $input['driver_back_license'] != '' ?
                $input['driver_back_license']->move('drivers/' . $user->id, $input['driver_back_license']->getClientOriginalName()) : '',

            'driver_front_license' => $input['driver_front_license'] != '' ?
                $input['driver_front_license']->move('drivers/' . $user->id, $input['driver_front_license']->getClientOriginalName()) : '',

            'ID_front' => $input['ID_front'] != '' ?
                $input['ID_front']->move('drivers/' . $user->id, $input['ID_front']->getClientOriginalName()) : '',

            'ID_back' => $input['ID_back'] != '' ?
                $input['ID_back']->move('drivers/' . $user->id, $input['ID_back']->getClientOriginalName()) : '',

            'Vehicle_front_license' => $input['Vehicle_front_license'] != '' ?
                $input['Vehicle_front_license']->move('drivers/' . $user->id, $input['Vehicle_front_license']->getClientOriginalName()) : '',

            'Vehicle_back_license' => $input['Vehicle_back_license'] != '' ?
                $input['Vehicle_back_license']->move('drivers/' . $user->id, $input['Vehicle_back_license']->getClientOriginalName()) : '',
        ]);
    }

    public function edit_user_market_role($id,$input)
    {
        $driver = Driver::where('user_id',$id)->first();
        $updated = Driver::where('id',$driver->id)->update([
            'birth_of_date' => $input['birth_of_date'],
            'nationalID' => $input['nationalID'],

            'driver_back_license' => $input['driver_back_license'] != '' ?
                $input['driver_back_license']->move('drivers/' . $id, $input['driver_back_license']->getClientOriginalName()) : '',

            'driver_front_license' => $input['driver_front_license'] != '' ?
                $input['driver_front_license']->move('drivers/' . $id, $input['driver_front_license']->getClientOriginalName()) : '',

            'ID_front' => $input['ID_front'] != '' ?
                $input['ID_front']->move('drivers/' . $id, $input['ID_front']->getClientOriginalName()) : '',

            'ID_back' => $input['ID_back'] != '' ?
                $input['ID_back']->move('drivers/' . $id, $input['ID_back']->getClientOriginalName()) : '',

            'Vehicle_front_license' => $input['Vehicle_front_license'] != '' ?
                $input['Vehicle_front_license']->move('drivers/' . $id, $input['Vehicle_front_license']->getClientOriginalName()) : '',

            'Vehicle_back_license' => $input['Vehicle_back_license'] != '' ?
                $input['Vehicle_back_license']->move('drivers/' . $id, $input['Vehicle_back_license']->getClientOriginalName()) : '',
            ]);
    }
}
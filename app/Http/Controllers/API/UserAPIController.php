<?php
/**
 * File name: UserAPIController.php
 * Last modified: 2020.10.29 at 17:03:54
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\CustomFieldRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Prettus\Validator\Exceptions\ValidatorException;


class UserAPIController extends Controller
{
    private $userRepository;
    private $uploadRepository;
    private $roleRepository;
    private $customFieldRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, UploadRepository $uploadRepository, RoleRepository $roleRepository, CustomFieldRepository $customFieldRepo)
    {
        $this->userRepository = $userRepository;
        $this->uploadRepository = $uploadRepository;
        $this->roleRepository = $roleRepository;
        $this->customFieldRepository = $customFieldRepo;
    }

    function login(Request $request)
    {
        try {
            $this->validate($request, [
                'phone' => 'required',
                'password' => 'required',
            ]);
            if (auth()->attempt(['phone' => $request->input('phone'), 'password' => $request->input('password')])) {
                // Authentication passed...
                $data=array();
                $user = auth()->user();
                $user->device_token = $request->input('device_token', '');
                $user->save();
                $data['token']=$user->api_token;
                $data['name']=$user->name;
                $data['has_media']=$user->has_media;
                $data['media']=$user->media;
                return $this->sendResponse($data, 'User retrieved successfully');
            }else{
                return $this->sendError('invalid data', 200);
            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 200);
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return
     */
    function register(Request $request)
    {
        $data=array();
        try {
            $this->validate($request, [
                'name' => 'required',
                'phone' => 'required|unique:users',
                'password' => 'required',
            ]);
            $user = new User;
            $user->name = $request->input('name');
            $user->phone = $request->input('phone');
            $user->device_token = $request->input('device_token', '');
            $user->password = Hash::make($request->input('password'));
            $user->api_token = str_random(60);
            $user->save();

            $defaultRoles = $this->roleRepository->findByField('default', '1');
            $defaultRoles = $defaultRoles->pluck('name')->toArray();
            $user->assignRole($defaultRoles);


            $data['token']=$user->api_token;
            $data['name']=$user->name;
            $data['has_media']=$user->has_media;
            $data['media']=$user->media;

//            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

//            foreach (getCustomFieldsValues($customFields, $request) as $value) {
//                $user->customFieldsValues()
//                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
//            }


//            if (copy(public_path('images/avatar_default.png'), public_path('images/avatar_default_temp.png'))) {
//                $user->addMedia(public_path('images/avatar_default_temp.png'))
//                    ->withCustomProperties(['uuid' => bcrypt(str_random())])
//                    ->toMediaCollection('avatar');
//            }
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 200);
        }
        return $this->sendResponse($data, 'User retrieved successfully');
    }

    function logout(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();
        if (!$user) {
            return $this->sendError('User not found', 401);
        }
        try {
            auth()->logout();
        } catch (\Exception $e) {
            $this->sendError($e->getMessage(), 401);
        }
        return $this->sendResponse($user['name'], 'User logout successfully');

    }

    function user(Request $request)
    {
        $user = $this->userRepository->findByField('api_token', $request->input('api_token'))->first();

        if (!$user) {
            return $this->sendError('User not found', 401);
        }

        return $this->sendResponse($user, 'User retrieved successfully');
    }

    function settings(Request $request)
    {
        $settings = setting()->all();
        $settings = array_intersect_key($settings,
            [
                'default_tax' => '',
                'default_currency' => '',
                'default_currency_decimal_digits' => '',
                'app_name' => '',
                'currency_right' => '',
                'enable_paypal' => '',
                'enable_stripe' => '',
                'enable_razorpay' => '',
                'main_color' => '',
                'main_dark_color' => '',
                'second_color' => '',
                'second_dark_color' => '',
                'accent_color' => '',
                'accent_dark_color' => '',
                'scaffold_dark_color' => '',
                'scaffold_color' => '',
                'google_maps_key' => '',
                'fcm_key' => '',
                'mobile_language' => '',
                'app_version' => '',
                'enable_version' => '',
                'distance_unit' => '',
                'home_section_1'=> '',
                'home_section_2'=> '',
                'home_section_3'=> '',
                'home_section_4'=> '',
                'home_section_5'=> '',
                'home_section_6'=> '',
                'home_section_7'=> '',
                'home_section_8'=> '',
                'home_section_9'=> '',
                'home_section_10'=> '',
                'home_section_11'=> '',
                'home_section_12'=> '',
            ]
        );

        if (!$settings) {
            return $this->sendError('Settings not found', 401);
        }

        return $this->sendResponse($settings, 'Settings retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param Request $request
     *
     */
    public function update($id, Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendResponse([
                'error' => true,
                'code' => 404,
            ], 'User not found');
        }
        $input = $request->except(['password', 'api_token']);
        try {
            if ($request->has('device_token')) {
                $user = $this->userRepository->update($request->only('device_token'), $id);
            } else {
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
                if (isset($input['avatar']) && $input['avatar']) {
                    $cacheUpload = $this->uploadRepository->getByUuid($input['avatar']);
                    $mediaItem = $cacheUpload->getMedia('avatar')->first();
                    if ($user->hasMedia('avatar')) {
                        $user->getFirstMedia('avatar')->delete();
                    }
                    $mediaItem->copy($user, 'avatar');
                }
                $user = $this->userRepository->update($input, $id);

                foreach (getCustomFieldsValues($customFields, $request) as $value) {
                    $user->customFieldsValues()
                        ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
                }
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage(), 401);
        }

        return $this->sendResponse($user, __('lang.updated_successfully', ['operator' => __('lang.user')]));
    }

    function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return $this->sendResponse(true, 'Reset link was sent successfully');
        } else {
            return $this->sendError('Reset link not sent', 401);
        }

    }

    public function checkPhone(Request $request){
        try {
            $this->validate($request, [
                'phone' => 'required|unique:users',
            ]);
            return $this->sendResponse([], 'successfully');

        } catch (\Exception $e) {
            return $this->sendError('phone already exist', 200);
        }
    }

    public function checkVerification(Request $request){
        try {
            $this->validate($request, [
                'phone' => 'required',
                'code' => 'required',
            ]);
            if ($request->input('code')==1234){
                return $this->sendResponse([], 'successfully');
            }else{
                return $this->sendError('check code please', 200);
            }
        } catch (\Exception $e) {
            return $this->sendError('phone already exist', 200);
        }
    }

    public function forget_password(Request $request){
        try {
            $this->validate($request, [
                'phone' => 'required|unique:users',
            ]);
            return $this->sendError('phone not exist', 200);

        } catch (\Exception $e) {
            //send otp Code Here
            return $this->sendResponse([], 'successfully');

        }
    }

    public function update_password(Request $request)
    {
        try {
            $this->validate($request, [
                'phone' => 'required|unique:users',
                'password' => 'required',
            ]);
            return $this->sendError('phone not exist', 200);
        } catch (\Exception $e) {
            $user=User::where('phone',$request->input('phone'))->first();
            $user->password = Hash::make($request->input('password'));
            $user->save();
            return $this->sendResponse([], 'successfully');

        }
    }
}

<?php
/**
 * File name: UserController.php
 * Last modified: 2020.05.04 at 12:15:13
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Events\UserRoleChangedEvent;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Driver;
use App\Models\Market;
use App\Models\MarketDay;
use App\Models\User;
use App\Repositories\CustomFieldRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use App\Traits\UserTraits;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class UserController extends Controller
{
    use UserTraits;
    /** @var  UserRepository */
    private $userRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    private $uploadRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    public function __construct(UserRepository $userRepo, RoleRepository $roleRepo, UploadRepository $uploadRepo,
                                CustomFieldRepository $customFieldRepo)
    {
        parent::__construct();
        $this->userRepository = $userRepo;
        $this->roleRepository = $roleRepo;
        $this->uploadRepository = $uploadRepo;
        $this->customFieldRepository = $customFieldRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('settings.users.index');
    }

    /**
     * Display a user profile.
     *
     * @param
     * @return Response
     */
    public function profile()
    {
        $user = $this->userRepository->findWithoutFail(auth()->id());
        unset($user->password);
        $customFields = false;
        $role = $this->roleRepository->pluck('name', 'name');
        $rolesSelected = $user->getRoleNames()->toArray();
        $customFieldsValues = $user->customFieldsValues()->with('customField')->get();
        //dd($customFieldsValues);
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
            $customFields = generateCustomField($customFields, $customFieldsValues);
        }
        return view('settings.users.profile', compact(['user', 'role', 'rolesSelected', 'customFields', 'customFieldsValues']));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $role = $this->roleRepository->pluck('name', 'name');
        $rolesSelected = [];
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            if (auth()->user()->hasRole('superadmin')) {
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model())->where('role','admin');
            }else{
                if (auth()->user()->hasRole('admin')) {
                    $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model())->where('role', 'market');
                }
                if (auth()->user()->hasRole('market')) {
                    $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model())->where('role', 'driver');
                }
            }
            $html = generateCustomField($customFields);
        }

        if (auth()->user()->hasRole('superadmin')) {
            return view('settings.users.superadmin.create')
                ->with("role", $role)
                ->with("customFields", isset($html) ? $html : false)
                ->with("rolesSelected", $rolesSelected);
        }
        elseif (auth()->user()->hasRole('admin')) {

            return view('settings.users.admin.create')
                ->with("role", $role)
                ->with("customFields", isset($html) ? $html : false)
                ->with("rolesSelected", $rolesSelected);
        }
        elseif (auth()->user()->hasRole('market')) {

            return view('settings.users.market.create')
                ->with("role", $role)
                ->with("customFields", isset($html) ? $html : false)
                ->with("rolesSelected", $rolesSelected);
        }
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
            return redirect(route('users.index'));
        }
        $input = $request->all();
        if(auth()->user()->hasRole('superadmin')){

            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model())->where('role','admin');

            $input['roles'] = isset($input['roles']) ? $input['roles'] : 'admin';
//            $input['password'] = Hash::make($input['password']);
            $input['api_token'] = str_random(60);
            try {
                $user = $this->userRepository->create($input);
                $user->syncRoles($input['roles']);
                $user->customFieldsValues()->createMany(getCustomFieldsValues($customFields, $request));

                if (isset($input['logo']) && $input['logo']) {
                    $cacheUpload = $this->uploadRepository->getByUuid($input['logo']);
                    $mediaItem = $cacheUpload->getMedia('logo')->first();
                    $mediaItem->copy($user, 'logo');
                }
//                event(new UserRoleChangedEvent($user));
                if (isset($input['legal_attachment']) && $input['legal_attachment']) {
                    $cacheUpload1 = $this->uploadRepository->getByUuid($input['legal_attachment']);
                    $mediaItem1 = $cacheUpload1->getMedia('legal_attachment')->first();
                    $mediaItem1->copy($user, 'legal_attachment');
                }
                event(new UserRoleChangedEvent($user));
            } catch (ValidatorException $e) {
                Flash::error($e->getMessage());
            }
            Flash::success('the admin added successfully');

        }
        elseif(auth()->user()->hasRole('admin'))
        {
            $this->add_user_admin_role($request->all());
            Flash::success('the admin adding successfully');
        }
        elseif (auth()->user()->hasRole('market')) {
            $this->add_user_market_role($input);
            Flash::success('the driver adding successfully');
        }
        return redirect('users');
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('settings.users.profile')->with('user', $user);
    }

    public function loginAsUser(Request $request, $id)
    {
        $user = $this->userRepository->findWithoutFail($id);
        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.index'));
        }
        auth()->login($user, true);
        if (auth()->id() !== $user->id) {
            Flash::error('User not found');
        }
        return redirect(route('users.profile'));
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);
        unset($user->password);
        $html = false;
        $role = $this->roleRepository->pluck('name', 'name');
        $rolesSelected = $user->getRoleNames()->toArray();
        $customFieldsValues = $user->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());
        $hasCustomField = in_array($this->userRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }
        if(auth()->user()->hasRole('superadmin')) {
            return view('settings.users.superadmin.edit')
                ->with('user', $user)->with("role", $role)
                ->with("rolesSelected", $rolesSelected)
                ->with("customFields", $html);
        }
        elseif(auth()->user()->hasRole('admin')){
            $market = $this->getUserMarket($id);
            return view('settings.users.admin.edit')
                ->with('user', $user)->with("role", $role)
                ->with("rolesSelected", $rolesSelected)
                ->with("customFields", $html)
                ->with("market",$market);
        }
        elseif (auth()->user()->hasRole('market')) {
            $driver = Driver::where('user_id',$id)->first();
            return view('settings.users.market.edit')
                ->with('user', $user)->with("role", $role)
                ->with("rolesSelected", $rolesSelected)
                ->with("customFields", $html)
                ->with("driver",$driver);
        }
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
            return redirect(route('users.profile'));
        }

        $user = $this->userRepository->findWithoutFail($id);


        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('users.profile'));
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->userRepository->model());

        $input = $request->all();
        if(auth()->user()->hasRole('superadmin')) {
            $input['roles'] = isset($input['roles']) ? $input['roles'] : 'admin';
            try {
                $user = $this->userRepository->update($input, $id);
                if (empty($user)) {
                    Flash::error('User not found');
                    return redirect(route('users.profile'));
                }
                if (isset($input['logo']) && $input['logo']) {
                    $cacheUpload = $this->uploadRepository->getByUuid($input['logo']);
                    $mediaItem = $cacheUpload->getMedia('logo')->first();
                    $mediaItem->copy($user, 'logo');
                }
//                event(new UserRoleChangedEvent($user));
                if (isset($input['legal_attachment']) && $input['legal_attachment']) {
                    $cacheUpload1 = $this->uploadRepository->getByUuid($input['legal_attachment']);
                    $mediaItem1 = $cacheUpload1->getMedia('legal_attachment')->first();
                    $mediaItem1->copy($user, 'legal_attachment');
                }
                if (auth()->user()->can('permissions.index')) {
                    $user->syncRoles($input['roles']);
                }
                foreach (getCustomFieldsValues($customFields, $request) as $value) {
                    $user->customFieldsValues()
                        ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
                }
                event(new UserRoleChangedEvent($user));
            } catch (ValidatorException $e) {
                Flash::error($e->getMessage());
            }


            Flash::success('User updated successfully.');
        }

        elseif(auth()->user()->hasRole('admin'))
        {
            $input['roles'] = isset($input['roles']) ? $input['roles'] : 'market';
            $user = $this->userRepository->update($input, $id);
            $this->edit_user_admin_role($input,$id);
            Flash::success('the admin updated successfully');
        }

        elseif (auth()->user()->hasRole('market')) {
            $input['roles'] = isset($input['roles']) ? $input['roles'] : 'market';
            $user = $this->userRepository->update($input, $id);
            $this->edit_user_market_role($input,$id);
            Flash::success('the driver updated successfully');
        }
        return redirect()->back();

    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
            return redirect(route('users.index'));
        }
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');
            Flash::error(trans('lang.user')." ".trans('not_found'));

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        //Flash::success('User deleted successfully.');
        Flash::success(trans('lang.deleted_successfully'));

        return redirect(route('users.index'));
    }

    /**
     * Remove Media of User
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        if (env('APP_DEMO', false)) {
            Flash::warning('This is only demo app you can\'t change this section ');
        } else {
            if (auth()->user()->can('medias.delete')) {
                $input = $request->all();
                $user = $this->userRepository->findWithoutFail($input['id']);
                try {
                    if ($user->hasMedia($input['collection'])) {
                        $user->getFirstMedia($input['collection'])->delete();
                    }
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        }
    }
}

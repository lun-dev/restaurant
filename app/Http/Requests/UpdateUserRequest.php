<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (auth()->user()->hasRole('superadmin')) {
            User::$rules['Market_name'] = 'required|string|min:1|max:15';
            User::$rules['password'] = 'required|string|min:6|same:password_confirmation';
            User::$rules['email'] = [
                'required',
//                'unique:users,email,id'.$this->get('id')
            ];
            User::$rules['phone'] = [
                'required',
//                'regex:/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/',
            ];
            User::$rules['National_ID'] = 'required|numeric|digits:10';
            User::$rules['Description'] = 'required|min:1|max:400';
        }
        elseif (auth()->user()->hasRole('admin')) {
            User::$rules['name'] = 'required|string|min:1|max:15';
            User::$rules['password'] = 'required|string|min:6|same:password_confirmation';
            User::$rules['email'] = [
                'required'
            ];
            User::$rules['phone'] = [
                'required',
//                'regex:/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/',
//                'unique:users'
            ];
        }
        return User::$rules;
    }
}

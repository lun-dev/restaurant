<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketDay extends Model
{
    protected $table = 'market_days';

    protected $fillable = [
        'market_id',
        'day_id',
        'start_from',
        'cloase_at',
    ];

    public function days(){
        return $this->belongsTo(Day::class,'day_id');
    }
}

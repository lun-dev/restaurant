-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 04, 2022 at 03:24 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lun_restaurantpos`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `key`, `value`) VALUES
(7, 'date_format', 'l jS F Y (H:i:s)'),
(8, 'language', 'en'),
(17, 'is_human_date_format', '1'),
(18, 'app_name', 'Smart Delivery'),
(19, 'app_short_description', 'Manage Mobile Application'),
(20, 'mail_driver', 'smtp'),
(21, 'mail_host', 'smtp.hostinger.com'),
(22, 'mail_port', '587'),
(23, 'mail_username', 'productdelivery@smartersvision.com'),
(24, 'mail_password', 'NnvAwk&&E7'),
(25, 'mail_encryption', 'ssl'),
(26, 'mail_from_address', 'productdelivery@smartersvision.com'),
(27, 'mail_from_name', 'Smarter Vision'),
(30, 'timezone', 'America/Montserrat'),
(32, 'theme_contrast', 'light'),
(33, 'theme_color', 'primary'),
(34, 'app_logo', '020a2dd4-4277-425a-b450-426663f52633'),
(35, 'nav_color', 'navbar-light bg-white'),
(38, 'logo_bg_color', 'bg-white'),
(66, 'default_role', 'admin'),
(68, 'facebook_app_id', '518416208939727'),
(69, 'facebook_app_secret', '93649810f78fa9ca0d48972fee2a75cd'),
(71, 'twitter_app_id', 'twitter'),
(72, 'twitter_app_secret', 'twitter 1'),
(74, 'google_app_id', '527129559488-roolg8aq110p8r1q952fqa9tm06gbloe.apps.googleusercontent.com'),
(75, 'google_app_secret', 'FpIi8SLgc69ZWodk-xHaOrxn'),
(77, 'enable_google', '1'),
(78, 'enable_facebook', '1'),
(93, 'enable_stripe', '1'),
(94, 'stripe_key', 'pk_test_pltzOnX3zsUZMoTTTVUL4O41'),
(95, 'stripe_secret', 'sk_test_o98VZx3RKDUytaokX4My3a20'),
(101, 'custom_field_models.0', 'App\\Models\\User'),
(104, 'default_tax', '10'),
(107, 'default_currency', '$'),
(108, 'fixed_header', '0'),
(109, 'fixed_footer', '0'),
(110, 'fcm_key', 'AAAAHMZiAQA:APA91bEb71b5sN5jl-w_mmt6vLfgGY5-_CQFxMQsVEfcwO3FAh4-mk1dM6siZwwR3Ls9U0pRDpm96WN1AmrMHQ906GxljILqgU2ZB6Y1TjiLyAiIUETpu7pQFyicER8KLvM9JUiXcfWK'),
(111, 'enable_notifications', '1'),
(112, 'paypal_username', 'sb-z3gdq482047_api1.business.example.com'),
(113, 'paypal_password', 'JV2A7G4SEMLMZ565'),
(114, 'paypal_secret', 'AbMmSXVaig1ExpY3utVS3dcAjx7nAHH0utrZsUN6LYwPgo7wfMzrV5WZ'),
(115, 'enable_paypal', '1'),
(116, 'main_color', '#25D366'),
(117, 'main_dark_color', '#25D366'),
(118, 'second_color', '#043832'),
(119, 'second_dark_color', '#ccccdd'),
(120, 'accent_color', '#8c98a8'),
(121, 'accent_dark_color', '#9999aa'),
(122, 'scaffold_dark_color', '#2c2c2c'),
(123, 'scaffold_color', '#fafafa'),
(124, 'google_maps_key', 'AIzaSyAT07iMlfZ9bJt1gmGj9KhJDLFY8srI6dA'),
(125, 'mobile_language', 'en'),
(126, 'app_version', '2.0.0'),
(127, 'enable_version', '1'),
(128, 'default_currency_id', '1'),
(129, 'default_currency_code', 'USD'),
(130, 'default_currency_decimal_digits', '2'),
(131, 'default_currency_rounding', '0'),
(132, 'currency_right', '0'),
(133, 'home_section_1', 'search'),
(134, 'home_section_2', 'slider'),
(135, 'home_section_3', 'top_markets_heading'),
(136, 'home_section_4', 'top_markets'),
(137, 'home_section_5', 'trending_week_heading'),
(138, 'home_section_6', 'trending_week'),
(139, 'home_section_7', 'categories_heading'),
(140, 'home_section_8', 'categories'),
(141, 'home_section_9', 'popular_heading'),
(142, 'home_section_10', 'popular'),
(143, 'home_section_11', 'recent_reviews_heading'),
(144, 'home_section_12', 'recent_reviews'),
(145, 'home_section_1', 'search'),
(146, 'home_section_2', 'slider'),
(147, 'home_section_3', 'top_markets_heading'),
(148, 'home_section_4', 'top_markets'),
(149, 'home_section_5', 'trending_week_heading'),
(150, 'home_section_6', 'trending_week'),
(151, 'home_section_7', 'categories_heading'),
(152, 'home_section_8', 'categories'),
(153, 'home_section_9', 'popular_heading'),
(154, 'home_section_10', 'popular'),
(155, 'home_section_11', 'recent_reviews_heading'),
(156, 'home_section_12', 'recent_reviews');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_options`
--

CREATE TABLE `cart_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Vegetables', 'Numquam incidunt tenetur ea repudiandae. Ab nisi suscipit suscipit et esse dolorum vel. Explicabo nemo hic neque at fugit consequatur. Sed ea aperiam aliquid odio tempora reiciendis. Minima asperiores nesciunt qui quos voluptatum.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(2, 'Fish', 'Officia ut possimus minus omnis modi. Similique accusantium voluptas rem laudantium commodi. Praesentium ex quae cupiditate maiores praesentium temporibus assumenda. Illum ad culpa qui impedit sunt autem sit. Rerum nisi quam qui harum rerum.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(3, 'Medicines', 'Et corporis quod quia enim possimus provident quisquam. Ex blanditiis atque consequatur quo modi quasi vel excepturi. Sunt est consequatur rem itaque unde laboriosam ut. Dolor dolorum et accusantium illum est dignissimos reprehenderit. Architecto odio perspiciatis libero a in.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(4, 'Drinks', 'Quia dolorem occaecati in ut fuga voluptatem. Aliquam doloremque nemo corporis commodi qui corporis. Eos vero tenetur voluptatibus atque. Ut non a est. Et quos ipsam sed numquam ut facilis.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(5, 'Fruit', 'Ducimus nemo iusto voluptas nihil nostrum sit. Accusamus unde et non non consequatur dolorum fugit. Eius non recusandae ex ad labore sit. Inventore perspiciatis magni assumenda animi. Officiis praesentium eos odit.', '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(6, 'Fruit', 'Delectus dignissimos illo quae. Reprehenderit maiores quibusdam ut voluptates omnis corporis. Quam sed exercitationem rerum quo voluptas id. Optio accusamus consequatur cupiditate necessitatibus. Dolor necessitatibus mollitia doloribus incidunt porro dolores dolores.', '2022-01-04 10:36:16', '2022-01-04 10:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'percent',
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(63) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_digits` tinyint(3) UNSIGNED DEFAULT NULL,
  `rounding` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `code`, `decimal_digits`, `rounding`, `created_at`, `updated_at`) VALUES
(1, 'US Dollar', '$', 'USD', 2, 0, '2019-10-22 13:50:48', '2019-10-22 13:50:48'),
(2, 'Euro', '€', 'EUR', 2, 0, '2019-10-22 13:51:39', '2019-10-22 13:51:39'),
(3, 'Indian Rupee', 'টকা', 'INR', 2, 0, '2019-10-22 13:52:50', '2019-10-22 13:52:50'),
(4, 'Indonesian Rupiah', 'Rp', 'IDR', 0, 0, '2019-10-22 13:53:22', '2019-10-22 13:53:22'),
(5, 'Brazilian Real', 'R$', 'BRL', 2, 0, '2019-10-22 13:54:00', '2019-10-22 13:54:00'),
(6, 'Cambodian Riel', '៛', 'KHR', 2, 0, '2019-10-22 13:55:51', '2019-10-22 13:55:51'),
(7, 'Vietnamese Dong', '₫', 'VND', 0, 0, '2019-10-22 13:56:26', '2019-10-22 13:56:26');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `in_table` tinyint(1) DEFAULT NULL,
  `bootstrap_column` tinyint(4) DEFAULT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `custom_field_model` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `name`, `type`, `values`, `disabled`, `required`, `in_table`, `bootstrap_column`, `order`, `custom_field_model`, `role`, `created_at`, `updated_at`) VALUES
(5, 'bio', 'textarea', NULL, 0, 0, 0, 6, 1, 'App\\Models\\User', NULL, '2019-09-06 19:43:58', '2019-09-06 19:43:58'),
(6, 'address', 'text', NULL, 0, 0, 0, 6, 3, 'App\\Models\\User', NULL, '2019-09-06 19:49:22', '2019-09-06 19:49:22'),
(7, 'verifiedPhone', 'text', NULL, 1, 0, 0, 6, 4, 'App\\Models\\User', NULL, '2021-03-20 08:49:22', '2021-03-20 08:49:22'),
(8, 'Market name', 'text', NULL, 0, 1, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:09:33', '2022-01-04 11:09:33'),
(9, 'phone', 'number', NULL, 0, 1, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:10:56', '2022-01-04 11:10:56'),
(10, 'National ID', 'text', NULL, 0, 1, 1, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:11:37', '2022-01-04 11:11:37'),
(11, 'Description', 'textarea', NULL, 0, 1, 1, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:12:42', '2022-01-04 11:12:42'),
(12, 'Logo', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:15:32', '2022-01-04 11:15:32'),
(13, 'Fees for service', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:15:56', '2022-01-04 11:15:56'),
(14, 'Number of  branches', 'number', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:16:25', '2022-01-04 11:16:25'),
(17, 'Bank account', 'text', NULL, 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 11:19:45', '2022-01-04 11:19:45'),
(18, 'Payment gateway', 'select', '[\"yes\",\"no\"]', 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 12:01:24', '2022-01-04 12:01:24'),
(19, 'Delivery', 'select', '[\"yes\",\"no\"]', 0, 0, 0, 6, NULL, 'App\\Models\\User', 'admin', '2022-01-04 12:02:24', '2022-01-04 12:02:24');

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_values`
--

CREATE TABLE `custom_field_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_field_id` int(10) UNSIGNED NOT NULL,
  `customizable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customizable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_field_values`
--

INSERT INTO `custom_field_values` (`id`, `value`, `view`, `custom_field_id`, `customizable_type`, `customizable_id`, `created_at`, `updated_at`) VALUES
(30, 'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.', 'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.', 5, 'App\\Models\\User', 2, '2019-09-06 19:52:30', '2019-10-16 17:32:35'),
(31, '2911 Corpening Drive South Lyon, MI 48178', '2911 Corpening Drive South Lyon, MI 48178', 6, 'App\\Models\\User', 2, '2019-09-06 19:52:30', '2019-10-16 17:32:35'),
(33, 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 5, 'App\\Models\\User', 1, '2019-09-06 19:53:58', '2019-10-16 17:23:53'),
(34, '569 Braxton Street Cortland, IL 60112', '569 Braxton Street Cortland, IL 60112', 6, 'App\\Models\\User', 1, '2019-09-06 19:53:58', '2019-10-16 17:23:53'),
(36, 'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse', 'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse', 5, 'App\\Models\\User', 3, '2019-10-15 15:21:32', '2019-10-17 21:21:12'),
(37, '1850 Big Elm Kansas City, MO 64106', '1850 Big Elm Kansas City, MO 64106', 6, 'App\\Models\\User', 3, '2019-10-15 15:21:32', '2019-10-17 21:21:43'),
(39, 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta', 5, 'App\\Models\\User', 4, '2019-10-16 17:31:46', '2019-10-16 17:31:46'),
(40, '1050 Frosty Lane Sidney, NY 13838', '1050 Frosty Lane Sidney, NY 13838', 6, 'App\\Models\\User', 4, '2019-10-16 17:31:46', '2019-10-16 17:31:46'),
(42, '<p>Short Bio</p>', 'Short Bio', 5, 'App\\Models\\User', 5, '2019-12-15 16:49:44', '2019-12-15 16:49:44'),
(43, '4722 Villa Drive', '4722 Villa Drive', 6, 'App\\Models\\User', 5, '2019-12-15 16:49:44', '2019-12-15 16:49:44'),
(45, '<p>Short bio for this driver</p>', 'Short bio for this driver', 5, 'App\\Models\\User', 6, '2020-03-29 15:28:05', '2020-03-29 15:28:05'),
(46, '4722 Villa Drive', '4722 Villa Drive', 6, 'App\\Models\\User', 6, '2020-03-29 15:28:05', '2020-03-29 15:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_addresses`
--

CREATE TABLE `delivery_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_addresses`
--

INSERT INTO `delivery_addresses` (`id`, `description`, `address`, `latitude`, `longitude`, `is_default`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Hic ducimus architecto dolore asperiores cumque.', '1299 Ariane Circle Suite 780\nArleneberg, VT 98183', '-75.320328', '-35.926565', 0, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 'Doloribus reprehenderit qui aperiam architecto neque.', '6499 Alexanne Flat Suite 240\nSchroederborough, DC 75750', '-75.920768', '62.881131', 0, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 'Voluptatibus qui molestiae ut sit aut sed qui.', '9040 Violet Spur\nNorth Elianeton, MD 42927', '88.909997', '-153.045277', 0, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 'Corrupti suscipit at reiciendis maxime.', '81813 Stroman Highway\nKosstown, TX 84972', '-39.33428', '-3.703866', 1, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 'Culpa fugit deserunt eos exercitationem minima eveniet et quia.', '86411 Viva Lake Suite 277\nWest Clifford, MO 72314', '41.109192', '-70.85215', 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 'Ducimus sed sed beatae iure voluptas.', '54358 Willa Plain Suite 635\nBinstown, MO 75676', '-12.635885', '-5.709121', 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, 'Ducimus laboriosam repellendus dolor perspiciatis qui.', '219 Rhianna Court\nPort Dangeloborough, CO 83848', '49.240793', '-82.055725', 0, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 'Inventore et voluptatem in.', '5586 Pouros Pines Apt. 262\nWest Candelario, AK 14178-6284', '-74.053639', '168.432087', 1, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 'Omnis quia voluptatibus aut.', '48933 Lehner Prairie\nEast Orenland, NE 27132', '-75.825923', '-45.977187', 1, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 'Et est quia animi eos omnis voluptas.', '84032 Kutch Valleys\nWest Ninastad, NC 64710', '74.874106', '123.32024', 1, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'Fugit dolores harum eaque sunt harum esse corporis.', '202 Yasmeen Ferry\nGerlachville, IN 42447-4066', '-25.89909', '-78.121823', 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 'Numquam consectetur facilis voluptatem voluptatum aut ut.', '4265 Effertz Springs Suite 301\nPort Noemifurt, MO 14182-9116', '-5.275894', '-98.35084', 0, 6, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, 'Aut sapiente impedit sit.', '4560 Octavia Mission\nPort Kitty, DE 96206', '-56.57517', '-19.852978', 1, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, 'Minus quo perspiciatis et.', '4515 Kaitlin Valleys\nLake Pascalemouth, NM 16590-8481', '43.419824', '105.653773', 0, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'Necessitatibus est illo a mollitia temporibus facilis beatae.', '800 Jamal Ramp\nRowebury, IN 85374-4669', '-11.474496', '-59.584587', 0, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `discountables`
--

CREATE TABLE `discountables` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `discountable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discountable_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `delivery_fee` double(5,2) NOT NULL DEFAULT 0.00,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `earning` double(9,2) NOT NULL DEFAULT 0.00,
  `available` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers_payouts`
--

CREATE TABLE `drivers_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `driver_markets`
--

CREATE TABLE `driver_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_markets`
--

INSERT INTO `driver_markets` (`user_id`, `market_id`) VALUES
(5, 1),
(5, 2),
(5, 4),
(6, 2),
(6, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `earnings`
--

CREATE TABLE `earnings` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `total_orders` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `total_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `admin_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `market_earning` double(9,2) NOT NULL DEFAULT 0.00,
  `delivery_fee` double(9,2) NOT NULL DEFAULT 0.00,
  `tax` double(9,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faq_category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `faq_category_id`, `created_at`, `updated_at`) VALUES
(1, 'Facilis velit nesciunt esse aperiam. Ut porro mollitia eius dolorum dolorum aliquam nihil.', 'She\'ll get me executed, as sure as ferrets are ferrets! Where CAN I have ordered\'; and she dropped it hastily, just in time to wash the things between whiles.\' \'Then you should say what you mean,\'.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 'Alias neque voluptatum consectetur aut est. Rem rerum commodi fugit nihil velit doloremque est aut.', 'I shall think nothing of tumbling down stairs! How brave they\'ll all think me at all.\' \'In that case,\' said the Cat, and vanished. Alice was not a moment to think that there was nothing so VERY.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 'Earum fugiat ut veniam. Autem harum ut perspiciatis et nam. Ex iste amet illo dignissimos et et.', 'March Hare. Visit either you like: they\'re both mad.\' \'But I don\'t know what they\'re like.\' \'I believe so,\' Alice replied eagerly, for she felt certain it must be what he did it,) he did not like.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 'Sint suscipit quos ea. Suscipit tenetur dolorum laudantium illo qui facilis.', 'I got up and said, \'That\'s right, Five! Always lay the blame on others!\' \'YOU\'D better not do that again!\' which produced another dead silence. \'It\'s a pun!\' the King hastily said, and went on: \'But.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 'Ut dolorum at et ut atque dicta accusantium. Cumque dolorem deserunt possimus.', 'Alice heard the Queen ordering off her head!\' Alice glanced rather anxiously at the righthand bit again, and did not come the same as they were gardeners, or soldiers, or courtiers, or three of the.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 'Omnis qui et enim nesciunt qui. Cumque maxime ratione at aut. Nemo ut est ea iste qui.', 'Alice thought she had forgotten the words.\' So they had been all the right words,\' said poor Alice, who was reading the list of the crowd below, and there was no more to be a Caucus-race.\' \'What IS.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, 'Enim qui laudantium suscipit repellat assumenda quibusdam. Quod inventore eos et quis quisquam.', 'I used--and I don\'t take this young lady tells us a story.\' \'I\'m afraid I can\'t understand it myself to begin with; and being ordered about by mice and rabbits. I almost think I should understand.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 'Non aperiam quae illo voluptatum aliquam pariatur facilis dicta. Voluptas rerum odit hic quia.', 'Alice in a low, timid voice, \'If you didn\'t like cats.\' \'Not like cats!\' cried the Mock Turtle, suddenly dropping his voice; and the words a little, \'From the Queen. \'Their heads are gone, if it had.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 'Repellat sit dolorem molestias omnis voluptate repellendus aut. Dolor a impedit hic in.', 'Shark, But, when the White Rabbit read:-- \'They told me he was speaking, so that by the way, was the Cat went on, half to Alice. \'Nothing,\' said Alice. \'Exactly so,\' said Alice. \'Why not?\' said the.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 'Cupiditate esse et mollitia doloribus. Quia voluptates aliquid tenetur error quas similique.', 'Dormouse shall!\' they both cried. \'Wake up, Alice dear!\' said her sister; \'Why, what are YOUR shoes done with?\' said the Cat, and vanished. Alice was very fond of pretending to be two people! Why.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'Voluptates adipisci illo eligendi tempora voluptatem a sunt. Dignissimos ut magni veniam odit.', 'Caterpillar. This was not going to dive in among the trees under which she had wept when she had but to her to speak again. The rabbit-hole went straight on like a wild beast, screamed \'Off with his.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 'Saepe et aliquid earum et nesciunt doloremque. Ipsa autem ut quis repellendus.', 'I shall have to fly; and the other two were using it as far as they would die. \'The trial cannot proceed,\' said the Dormouse go on with the grin, which remained some time with one eye; but to her.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, 'Qui labore aut voluptatem ab esse. Illo dolores dicta autem et nihil.', 'NOT, being made entirely of cardboard.) \'All right, so far,\' thought Alice, \'they\'re sure to happen,\' she said this, she was not a bit of stick, and held it out again, and all the children she knew.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, 'Labore sequi amet quia iure. Nesciunt qui accusantium ut omnis. Hic eum numquam est.', 'Alice soon came to the shore, and then sat upon it.) \'I\'m glad they\'ve begun asking riddles.--I believe I can reach the key; and if it had gone. \'Well! I\'ve often seen them so often, you know.\' \'Not.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'Officia et cum et blanditiis omnis veritatis ad. Animi in laudantium iure aut quod est soluta.', 'At this moment Alice felt a little wider. \'Come, it\'s pleased so far,\' thought Alice, \'it\'ll never do to hold it. As soon as the Caterpillar decidedly, and he hurried off. Alice thought she had to.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(16, 'Pariatur rerum tempore deserunt nihil et reprehenderit. Est molestiae qui at quam saepe atque est.', 'I do so like that curious song about the crumbs,\' said the Hatter: \'but you could keep it to his ear. Alice considered a little, half expecting to see that the Queen had ordered. They very soon had.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(17, 'Ea fuga non repudiandae magnam maiores accusamus. Molestiae unde repudiandae aut voluptatem et.', 'Soup, so rich and green, Waiting in a hurried nervous manner, smiling at everything about her, to pass away the time. Alice had never heard it before,\' said Alice,) and round the hall, but they all.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(18, 'Magnam alias repudiandae quos ea nam ad dolores. Aliquam esse nihil minima eum.', 'ME, and told me he was obliged to have been a RED rose-tree, and we put a white one in by mistake; and if it thought that SOMEBODY ought to have wondered at this, but at the moment, \'My dear! I.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(19, 'Et non expedita quam. Ut necessitatibus sequi dolorem commodi.', 'Lory, as soon as she tucked it away under her arm, with its mouth again, and put it in with the clock. For instance, if you want to see it again, but it makes rather a complaining tone, \'and they.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(20, 'Officia fugit vel qui tempora aut dolorem. Dignissimos veritatis illum vel est magnam minima.', 'Caterpillar. Alice thought decidedly uncivil. \'But perhaps he can\'t help it,\' said the Gryphon: and it said in a deep, hollow tone: \'sit down, both of you, and must know better\'; and this was his.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(21, 'Repellendus quia quas ratione repudiandae quo sit. Cum quis fugit animi quisquam aut distinctio.', 'There was exactly three inches high). \'But I\'m not Ada,\' she said, without even waiting to put it into his cup of tea, and looked very uncomfortable. The moment Alice appeared, she was dozing off.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(22, 'Explicabo est sint aut iusto ipsam eaque. Quia ratione voluptatem non itaque voluptatem saepe fuga.', 'Time as well she might, what a wonderful dream it had lost something; and she felt sure she would manage it. \'They must go back by railway,\' she said to the Knave \'Turn them over!\' The Knave did so.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(23, 'Nostrum dolore non veniam quisquam. Magni dolores sit molestiae temporibus voluptas sint sit.', 'I THINK,\' said Alice. \'Who\'s making personal remarks now?\' the Hatter went on all the first to break the silence. \'What day of the house if it please your Majesty,\' said the Duck. \'Found IT,\' the.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(24, 'Sunt iusto atque nesciunt molestiae officiis. Facere provident rerum quam maiores eaque.', 'YOU sing,\' said the Duck. \'Found IT,\' the Mouse was bristling all over, and both the hedgehogs were out of the evening, beautiful Soup! Soup of the game, feeling very glad that it made no mark; but.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(25, 'Qui cum alias voluptatem aut ipsam ipsum cum quia. Quia possimus tempora et rerum ut velit.', 'Alice, who felt very lonely and low-spirited. In a minute or two, and the words a little, \'From the Queen. \'You make me smaller, I can guess that,\' she added in an impatient tone: \'explanations take.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(26, 'Delectus et consectetur sed cumque sint quia. Et iusto a pariatur officiis alias aut facere.', 'Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, but in a tone of great relief. \'Call the first witness,\' said the Mock Turtle in a very long silence, broken only by.', 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(27, 'Accusantium minus dicta rerum sint. Quo maiores eaque a nisi quo et et.', 'Alice in a whisper.) \'That would be a walrus or hippopotamus, but then she had not the smallest idea how confusing it is all the arches are gone from this morning,\' said Alice sadly. \'Hand it over a.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(28, 'Hic libero illum dolores ab vitae animi. Nihil fugit ea tempore totam. Enim ex sunt quas nam.', 'Seaography: then Drawling--the Drawling-master was an uncomfortably sharp chin. However, she got into a chrysalis--you will some day, you know--and then after that savage Queen: so she waited. The.', 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(29, 'Eos impedit a corrupti. Nam esse ipsum maiores non eius repudiandae.', 'It was all ridges and furrows; the balls were live hedgehogs, the mallets live flamingoes, and the soldiers shouted in reply. \'That\'s right!\' shouted the Queen, who was beginning to write out a.', 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(30, 'Incidunt qui repellat voluptas soluta numquam. Ut quis suscipit sunt aut dolorem.', 'Hatter, \'when the Queen shrieked out. \'Behead that Dormouse! Turn that Dormouse out of its mouth again, and put it to make herself useful, and looking at it uneasily, shaking it every now and then.', 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `faq_categories`
--

CREATE TABLE `faq_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq_categories`
--

INSERT INTO `faq_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Products', '2019-08-31 10:31:52', '2019-08-31 10:31:52'),
(2, 'Services', '2019-08-31 10:32:03', '2019-08-31 10:32:03'),
(3, 'Delivery', '2019-08-31 10:32:11', '2019-08-31 10:32:11'),
(4, 'Misc', '2019-08-31 10:32:17', '2019-08-31 10:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 13, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 12, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 14, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 19, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 5, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, 6, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 20, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 25, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 20, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 26, 6, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 10, 6, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, 14, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, 6, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 10, 6, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(16, 12, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(17, 3, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(18, 13, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(19, 5, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(20, 16, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(21, 19, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(22, 18, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(23, 27, 6, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(24, 6, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(25, 21, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(26, 23, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(27, 29, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(28, 29, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(29, 27, 6, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(30, 25, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_options`
--

CREATE TABLE `favorite_options` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `favorite_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorite_options`
--

INSERT INTO `favorite_options` (`option_id`, `favorite_id`) VALUES
(1, 1),
(1, 5),
(2, 6),
(3, 2),
(5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Grocery', 'Eum similique maiores atque quia explicabo. Dolores quia placeat consequatur id quis perspiciatis. Ducimus sit ducimus officia labore maiores et porro. Est iusto natus nesciunt debitis consequuntur totam. Et illo et autem inventore earum corrupti.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(2, 'Pharmacy', 'Eaque et aut natus. Minima blanditiis ut sunt distinctio ad. Quasi doloremque rerum ex rerum. Molestias similique similique aut rerum delectus blanditiis et. Dolorem et quas nostrum est nobis.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(3, 'Restaurant', 'Est nihil omnis natus ducimus ducimus excepturi quos. Et praesentium in quia veniam. Tempore aut nesciunt consequatur pariatur recusandae. Voluptatem commodi eius quaerat est deleniti impedit. Qui quo harum est sequi incidunt labore eligendi cum.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(4, 'Store', 'Ex nostrum suscipit aut et labore. Ut dolor ut eum eum voluptatem ex. Sapiente in tempora soluta voluptatem. Officia accusantium quae sit. Rerum esse ipsa molestias dolorem et est autem consequatur.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(5, 'Electronics', 'Dolorum earum ut blanditiis blanditiis. Facere quis voluptates assumenda saepe. Ab aspernatur voluptatibus rem doloremque cum impedit. Itaque blanditiis commodi repudiandae asperiores. Modi atque placeat consectetur et aut blanditiis.', '2020-04-11 13:03:21', '2020-04-11 13:03:21'),
(6, 'Furniture', 'Est et iste enim. Quam repudiandae commodi rerum non esse. Et in aut sequi est aspernatur. Facere non modi expedita asperiores. Ipsa laborum saepe deserunt qui consequatur voluptas inventore dolorum.', '2020-04-11 13:03:21', '2020-04-11 13:03:21');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `description`, `market_id`, `created_at`, `updated_at`) VALUES
(1, 'Quas quis aut rem vel.', 8, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(2, 'Repellendus atque neque aut itaque similique tempore a.', 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(3, 'Illum aut fugiat asperiores quia.', 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(4, 'Nihil sed eius in voluptatem pariatur.', 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(5, 'Minus delectus molestiae consequuntur non rerum quis quo.', 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(6, 'Deserunt sapiente nemo molestiae.', 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(7, 'Tempore sed hic est cumque.', 8, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(8, 'Vel qui iusto non laborum fugit qui.', 10, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(9, 'Atque tempore inventore et aut eos voluptas molestiae nemo.', 9, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(10, 'Assumenda aut necessitatibus voluptatem ea commodi enim reprehenderit quia.', 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(11, 'Nihil placeat consectetur consequatur qui commodi quod.', 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(12, 'Maxime veritatis aut culpa vel.', 9, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(13, 'Reiciendis qui earum omnis minima occaecati saepe explicabo.', 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(14, 'Rerum mollitia iusto et et reprehenderit ex.', 10, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(15, 'In optio laudantium ipsum est dolorem enim maiores.', 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(16, 'Eum fugiat non consequatur rerum veniam ea.', 4, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(17, 'Aut numquam qui est maiores impedit reprehenderit est.', 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(18, 'Non optio rerum enim.', 9, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(19, 'Temporibus velit in nihil quod vel nemo.', 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(20, 'Accusamus dicta libero voluptas velit fugit.', 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `markets`
--

CREATE TABLE `markets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_commission` double(8,2) DEFAULT 0.00,
  `delivery_fee` double(8,2) DEFAULT 0.00,
  `delivery_range` double(8,2) DEFAULT 0.00,
  `default_tax` double(8,2) DEFAULT 0.00,
  `closed` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 0,
  `available_for_delivery` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `markets`
--

INSERT INTO `markets` (`id`, `name`, `description`, `address`, `latitude`, `longitude`, `phone`, `mobile`, `information`, `admin_commission`, `delivery_fee`, `delivery_range`, `default_tax`, `closed`, `active`, `available_for_delivery`, `created_at`, `updated_at`) VALUES
(1, 'Mall Crona, Mraz and Lubowitz', 'Cum et ut blanditiis dolor error maiores. Iure porro sit nam quasi adipisci. Ipsum temporibus explicabo excepturi sed quod laudantium quis.', '117 Kelley Greens Suite 664\nKerluketon, GA 98110', '44.302767', '10.702792', '790.210.5189', '(298) 925-3512 x936', 'Adipisci quibusdam iure blanditiis. Omnis et minima eos omnis nobis voluptatum delectus repellat. Inventore suscipit aut corporis quos et voluptatibus.', 41.44, 6.13, 41.96, 10.42, 1, 1, 0, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(2, 'Market Becker-Howe', 'Cumque et neque facere sed sunt cum totam. Qui optio modi sit est excepturi. Sed expedita ad non inventore voluptas.', '379 Bayer Hollow\nFredatown, OH 07776-6716', '51.527097', '9.188774', '990-201-0982 x249', '(476) 505-0302', 'Voluptas enim alias blanditiis aliquam porro ex. Velit molestiae porro officia atque cumque enim minus. Aut aut eveniet numquam porro.', 20.70, 4.88, 75.39, 27.91, 1, 1, 0, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(3, 'Pharmacy Mills-Kuhn', 'Sunt voluptas sunt voluptas amet eaque voluptatibus. Accusantium et aliquam qui nulla doloribus. Quas ut explicabo ea sed architecto soluta dolorem. Harum a enim sed nulla et.', '737 Rath Springs\nLake Betsyton, NV 22216-6335', '50.311777', '8.33642', '820-209-9071', '958.759.5458 x49969', 'Molestias nulla tempora quibusdam iure quis eos itaque. Rerum assumenda modi dolore aperiam. Ut vero dolorem blanditiis consequuntur.', 20.73, 2.36, 55.45, 23.11, 0, 1, 0, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(4, 'Furniture Kreiger, Bayer and Ledner', 'Suscipit aut sed et velit et amet non. Tempore vel quia vitae nihil veritatis in sit. Aspernatur vel culpa et.', '41035 Alberta Vista Apt. 443\nWardtown, MT 01517-5206', '50.31873', '10.646104', '626-634-6813', '883-414-0777 x4128', 'Aperiam mollitia quo consequatur. Quo ea animi et repellat. Possimus velit animi ea alias laboriosam sit.', 41.44, 3.19, 31.33, 18.39, 1, 1, 0, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(5, 'Furniture Rempel, Wuckert and Pollich', 'Voluptatem veniam quis dolores veniam. Itaque nemo aut dolorum magnam asperiores at amet. Reprehenderit voluptate aut et aut molestiae eius. Aut ut enim est sunt.', '954 Kassandra River Suite 161\nConnfort, AZ 78231', '54.153167', '9.518548', '(330) 370-9204', '547-506-7931 x531', 'Alias enim voluptatem autem dolor aut quaerat dolorum corporis. Eius vitae alias ratione ipsam ipsam illum. Minima totam consectetur sit ut distinctio.', 37.10, 8.90, 68.25, 28.09, 0, 1, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(6, 'Furniture Tremblay Group', 'Est nemo perferendis ea. Suscipit non reiciendis a eveniet. Est rerum et perspiciatis.', '9467 Reilly Circle\nAminamouth, DC 77608-9968', '43.918172', '9.04389', '1-341-279-0441 x393', '+1.409.232.0608', 'Id ab totam omnis debitis quibusdam qui ratione. Aut sit omnis quae occaecati et. Quae neque qui cupiditate deserunt consectetur harum adipisci.', 33.94, 1.26, 29.76, 6.21, 1, 1, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(7, 'Mall Schroeder, Dooley and Swift', 'Omnis cumque provident ex provident hic. Deserunt ea nobis eligendi totam natus temporibus. Et autem cupiditate deserunt et voluptatem minima voluptas dolorem. Reiciendis et fugit fuga quas eos.', '24948 Amara Lock\nJazmynborough, MN 29596-1395', '54.785162', '7.856362', '(579) 462-1102', '939-716-8206 x630', 'Qui eum aliquam repellat corrupti voluptatem blanditiis. Impedit vitae aliquid rem facere cumque quia. Cupiditate maiores itaque non ut.', 22.29, 3.52, 58.60, 17.50, 0, 1, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(8, 'Shop Brakus Ltd', 'Aliquid laborum quia esse rem distinctio corporis labore. Veniam qui doloribus sunt beatae id. Dolorem reiciendis ut tenetur enim. Unde laudantium qui consequuntur dolore ipsam dignissimos.', '710 Sydni Ferry Suite 277\nLake Pedro, CT 80952', '45.113081', '7.675477', '950.738.7550', '328-265-2273', 'Nesciunt facere itaque aut voluptas mollitia sit. Qui est corrupti qui praesentium sint. Dignissimos qui aspernatur id necessitatibus autem temporibus qui.', 47.80, 2.43, 61.77, 27.36, 1, 1, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(9, 'Market Wilkinson-Harvey', 'Quasi provident non aspernatur. Quaerat nihil minima sit beatae expedita consequatur iure et. Id omnis voluptatem odit provident vel cupiditate ea.', '63941 Crona Course\nFisherburgh, IN 57929', '37.245728', '9.895635', '1-452-620-0462 x9023', '+1-515-253-6020', 'Blanditiis saepe repudiandae deleniti tempora dolores. Qui deleniti maiores facere doloremque doloremque tenetur maxime. Provident facere ut qui.', 47.73, 5.68, 43.18, 23.45, 0, 1, 0, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(10, 'Furniture Hills, Thompson and Smith', 'Eum corporis praesentium ut ut error aliquam quo. Id commodi explicabo impedit. Veniam aut quae recusandae est. Odio fugit illo quam aut nihil.', '548 Aufderhar Corner Suite 067\nSouth Alvaton, VT 10990-5840', '47.019483', '10.215799', '337.468.0847', '(357) 283-3767', 'Quis distinctio dolore voluptatem maxime minus fuga soluta. Facere recusandae quia et eaque. Totam facere eius ut maiores sint iste.', 18.53, 5.06, 47.19, 25.91, 0, 1, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `markets_payouts`
--

CREATE TABLE `markets_payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT 0.00,
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `market_fields`
--

CREATE TABLE `market_fields` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `market_fields`
--

INSERT INTO `market_fields` (`field_id`, `market_id`) VALUES
(1, 7),
(1, 9),
(2, 1),
(2, 2),
(2, 7),
(3, 2),
(3, 6),
(4, 1),
(4, 3),
(5, 8),
(5, 10),
(6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `market_reviews`
--

CREATE TABLE `market_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `market_reviews`
--

INSERT INTO `market_reviews` (`id`, `review`, `rate`, `user_id`, `market_id`, `created_at`, `updated_at`) VALUES
(1, 'Gryphon, and the Gryphon never learnt it.\' \'Hadn\'t time,\' said the Mock Turtle in the act of crawling away: besides all this, there was enough of me left to make out who I am! But I\'d better take.', 5, 4, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 'So she tucked her arm affectionately into Alice\'s, and they lived at the end of the game, feeling very glad to find herself talking familiarly with them, as if he wasn\'t one?\' Alice asked. The.', 2, 5, 8, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 'Alice said very politely, \'for I never understood what it was: she was playing against herself, for this curious child was very nearly getting up and walking off to other parts of the conversation.', 4, 2, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 'Alice\'s head. \'Is that all?\' said the Queen, the royal children; there were any tears. No, there were ten of them, and just as well. The twelve jurors were all talking together: she made it out.', 4, 2, 10, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 'King hastily said, and went stamping about, and shouting \'Off with her head! Off--\' \'Nonsense!\' said Alice, swallowing down her anger as well go back, and see what this bottle was a dispute going on.', 3, 2, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 'Mock Turtle, capering wildly about. \'Change lobsters again!\' yelled the Gryphon in an offended tone, \'was, that the poor little thing grunted in reply (it had left off when they hit her; and when.', 1, 2, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, 'Dinn may be,\' said the Cat. \'I said pig,\' replied Alice; \'and I do hope it\'ll make me smaller, I can listen all day about it!\' and he poured a little shriek, and went back to finish his story.', 3, 3, 9, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 'White Rabbit, with a great deal to ME,\' said Alice in a court of justice before, but she got up, and began smoking again. This time Alice waited till she had quite a commotion in the same tone.', 3, 2, 7, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 'As they walked off together, Alice heard the Rabbit came up to her great disappointment it was the first to speak. \'What size do you know about it, you know.\' He was looking down at once, in a tone.', 5, 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 'There was certainly English. \'I don\'t see any wine,\' she remarked. \'It tells the day of the hall; but, alas! the little golden key, and unlocking the door opened inwards, and Alice\'s first thought.', 4, 4, 10, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'First, she dreamed of little Alice was rather doubtful whether she could remember about ravens and writing-desks, which wasn\'t much. The Hatter was the first to speak. \'What size do you know about.', 2, 1, 7, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 'Hatter said, turning to Alice, and she went hunting about, and crept a little faster?\" said a whiting to a snail. \"There\'s a porpoise close behind us, and he\'s treading on her toes when they had.', 3, 4, 7, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, 'Seven flung down his brush, and had been to her, And mentioned me to him: She gave me a pair of gloves and a sad tale!\' said the Caterpillar, just as well as she picked her way into a butterfly, I.', 2, 5, 10, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, 'Cat. \'--so long as you go on? It\'s by far the most important piece of it at last, and they sat down, and felt quite relieved to see if she could not help thinking there MUST be more to do anything.', 2, 5, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'Normans--\" How are you thinking of?\' \'I beg your acceptance of this ointment--one shilling the box-- Allow me to him: She gave me a good character, But said I didn\'t!\' interrupted Alice. \'You must.', 4, 2, 9, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(16, 'Gryphon said, in a melancholy air, and, after glaring at her own mind (as well as she went on for some time busily writing in his note-book, cackled out \'Silence!\' and read as follows:-- \'The Queen.', 4, 6, 10, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(17, 'King. (The jury all looked so grave and anxious.) Alice could see, as well look and see after some executions I have none, Why, I do it again and again.\' \'You are not the right size, that it would.', 1, 1, 8, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(18, 'March Hare and the shrill voice of thunder, and people began running when they passed too close, and waving their forepaws to mark the time, while the Dodo said, \'EVERYBODY has won, and all of them.', 3, 2, 8, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(19, 'Gryphon never learnt it.\' \'Hadn\'t time,\' said the Hatter. He had been wandering, when a sharp hiss made her next remark. \'Then the Dormouse shook itself, and was going to dive in among the branches.', 1, 4, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(20, 'Hatter was the White Rabbit, \'but it seems to like her, down here, and I\'m sure she\'s the best plan.\' It sounded an excellent opportunity for making her escape; so she turned the corner, but the.', 4, 6, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_26_175145_create_permission_tables', 1),
(4, '2018_06_12_140344_create_media_table', 1),
(5, '2018_06_13_035117_create_uploads_table', 1),
(6, '2018_07_17_180731_create_settings_table', 1),
(7, '2018_07_24_211308_create_custom_fields_table', 1),
(8, '2018_07_24_211327_create_custom_field_values_table', 1),
(9, '2019_08_29_213820_create_fields_table', 1),
(10, '2019_08_29_213821_create_markets_table', 1),
(11, '2019_08_29_213822_create_categories_table', 1),
(12, '2019_08_29_213826_create_option_groups_table', 1),
(13, '2019_08_29_213829_create_faq_categories_table', 1),
(14, '2019_08_29_213833_create_order_statuses_table', 1),
(15, '2019_08_29_213837_create_products_table', 1),
(16, '2019_08_29_213838_create_options_table', 1),
(17, '2019_08_29_213842_create_galleries_table', 1),
(18, '2019_08_29_213847_create_product_reviews_table', 1),
(19, '2019_08_29_213921_create_payments_table', 1),
(20, '2019_08_29_213922_create_delivery_addresses_table', 1),
(21, '2019_08_29_213926_create_faqs_table', 1),
(22, '2019_08_29_213940_create_market_reviews_table', 1),
(23, '2019_08_30_152927_create_favorites_table', 1),
(24, '2019_08_31_111104_create_orders_table', 1),
(25, '2019_09_04_153857_create_carts_table', 1),
(26, '2019_09_04_153858_create_favorite_options_table', 1),
(27, '2019_09_04_153859_create_cart_options_table', 1),
(28, '2019_09_04_153958_create_product_orders_table', 1),
(29, '2019_09_04_154957_create_product_order_options_table', 1),
(30, '2019_09_04_163857_create_user_markets_table', 1),
(31, '2019_10_22_144652_create_currencies_table', 1),
(32, '2019_12_14_134302_create_driver_markets_table', 1),
(33, '2020_03_25_094752_create_drivers_table', 1),
(34, '2020_03_25_094802_create_earnings_table', 1),
(35, '2020_03_25_094809_create_drivers_payouts_table', 1),
(36, '2020_03_25_094817_create_markets_payouts_table', 1),
(37, '2020_03_27_094855_create_notifications_table', 1),
(38, '2020_04_11_135804_create_market_fields_table', 1),
(39, '2020_08_23_181022_create_coupons_table', 1),
(40, '2020_08_23_181029_create_discountables_table', 1),
(41, '2020_09_01_192732_create_slides_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\Models\\User', 1),
(3, 'App\\Models\\User', 2),
(4, 'App\\Models\\User', 3),
(4, 'App\\Models\\User', 4),
(5, 'App\\Models\\User', 5),
(5, 'App\\Models\\User', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `description`, `price`, `product_id`, `option_group_id`, `created_at`, `updated_at`) VALUES
(1, '500g', 'Laborum perferendis ab occaecati.', 43.59, 2, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 'Oil', 'Rerum laudantium et.', 36.26, 4, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 'XL', 'Inventore odit error sint amet vel.', 15.66, 24, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 'Tomato', 'Laboriosam dolorem recusandae voluptas cupiditate qui.', 30.46, 4, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 'Red', 'Id non tempore harum deserunt.', 17.45, 12, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, '500g', 'Et earum et ut.', 38.11, 11, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, '5L', 'Et veniam officia earum.', 33.14, 5, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 'S', 'Quisquam molestiae temporibus in.', 47.10, 29, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 'Oil', 'Quas cum non recusandae.', 20.77, 9, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 'Tomato', 'Omnis inventore saepe ullam voluptas nemo.', 30.20, 28, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'Red', 'Est eos rerum quibusdam quia.', 42.00, 15, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 'L', 'Tempora sint sed natus consequatur iste.', 40.91, 16, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, '1Kg', 'Nobis qui commodi impedit autem.', 34.74, 21, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, '500g', 'Ut maiores voluptatibus laborum.', 25.93, 25, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'Red', 'Non dolores suscipit ut accusantium asperiores.', 22.20, 6, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(16, 'Red', 'Tempora tempora sunt tempore delectus rerum.', 26.16, 28, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(17, 'Tomato', 'Natus et tenetur qui.', 39.90, 14, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(18, 'Oil', 'Laborum repellat praesentium deleniti nihil iusto.', 22.08, 21, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(19, 'Red', 'Eveniet voluptatem odit nostrum pariatur eum.', 37.06, 20, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(20, '1Kg', 'Ut provident consequuntur.', 47.56, 5, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(21, '500g', 'Temporibus accusantium laboriosam consequatur alias.', 12.50, 25, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(22, 'L', 'Saepe tempore id quas dolor.', 44.69, 26, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(23, 'S', 'Ut ut aut.', 37.85, 27, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(24, '1Kg', 'Odio magnam voluptas sit.', 20.40, 3, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(25, '1Kg', 'Aut magni non qui corporis.', 17.85, 25, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(26, 'XL', 'Ab hic asperiores alias.', 16.80, 18, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(27, 'XL', 'Fuga facilis vitae reprehenderit.', 47.82, 18, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(28, 'S', 'Dolores veritatis similique perspiciatis a.', 22.31, 11, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(29, 'Oil', 'Consequatur laudantium ut reiciendis suscipit.', 21.71, 26, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(30, 'Red', 'Autem consectetur maiores voluptates ut harum.', 20.86, 26, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(31, 'Oil', 'In recusandae facilis est delectus.', 16.89, 18, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(32, 'Red', 'Dicta qui eveniet porro vel.', 19.16, 3, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(33, 'S', 'Molestias doloremque culpa itaque.', 49.91, 29, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(34, 'XL', 'Nisi expedita voluptatem nulla autem aut.', 23.28, 26, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(35, 'L', 'Id ut saepe est possimus architecto.', 36.63, 27, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(36, '2L', 'Enim ipsum expedita similique.', 39.64, 29, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(37, 'Tomato', 'Nobis et provident qui saepe.', 24.91, 30, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(38, 'XL', 'Dolorum repellat nam adipisci.', 14.12, 29, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(39, 'Red', 'Quas maxime explicabo vitae.', 43.25, 17, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(40, '1Kg', 'Qui similique ut at.', 45.48, 9, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(41, 'XL', 'Numquam in saepe voluptatum.', 24.92, 4, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(42, 'Red', 'Ullam porro accusamus cumque quas omnis.', 14.74, 1, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(43, 'Tomato', 'Non dolorem aliquam.', 22.08, 8, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(44, 'Red', 'Et sint recusandae et.', 48.45, 1, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(45, '2L', 'Recusandae qui repellendus dolore.', 37.09, 6, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(46, 'Tomato', 'Consequatur maiores beatae ipsa vel aspernatur.', 19.90, 8, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(47, 'S', 'Est itaque nulla quod.', 38.17, 28, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(48, 'Red', 'Qui voluptatem veritatis.', 12.98, 10, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(49, '1Kg', 'Rerum perspiciatis qui autem.', 36.49, 30, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(50, 'Oil', 'Harum ea id ex.', 12.31, 12, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(51, '5L', 'Et eos voluptate aut dolorem.', 22.13, 30, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(52, '500g', 'Et vitae deleniti.', 28.70, 8, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(53, 'Red', 'Voluptatibus dicta qui mollitia a.', 15.75, 28, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(54, 'Green', 'Quia et excepturi.', 48.50, 1, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(55, 'L', 'Saepe officia provident aliquid.', 23.00, 22, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(56, 'Green', 'Asperiores quae at.', 14.41, 30, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(57, 'Tomato', 'Quaerat cum totam quasi similique.', 43.14, 29, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(58, 'XL', 'Porro eos velit sunt reiciendis.', 13.60, 2, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(59, 'L', 'Placeat explicabo fugit ut.', 39.30, 6, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(60, '500g', 'Ex expedita ipsum exercitationem fugit aut.', 25.99, 10, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(61, 'White', 'Incidunt id et doloremque.', 16.86, 21, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(62, 'Oil', 'Vel explicabo est tenetur.', 49.88, 9, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(63, '500g', 'Tenetur eveniet eaque et tenetur.', 28.14, 26, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(64, 'Oil', 'Modi ratione est minima ut.', 14.44, 16, 2, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(65, '2L', 'Quae neque est aut.', 13.25, 21, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(66, '2L', 'Velit quaerat ducimus non nemo.', 42.68, 16, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(67, 'Red', 'Id quod eligendi alias.', 20.18, 20, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(68, 'Green', 'Impedit odit quis ut exercitationem.', 12.49, 15, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(69, 'White', 'Porro et numquam beatae ut nulla.', 31.97, 6, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(70, '2L', 'Dolorum earum eum explicabo.', 45.62, 2, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `option_groups`
--

CREATE TABLE `option_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_groups`
--

INSERT INTO `option_groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Size', '2019-08-31 08:55:28', '2019-08-31 08:55:28'),
(2, 'Color', '2019-10-09 11:26:28', '2019-10-09 11:26:28'),
(3, 'Parfum', '2019-10-09 11:26:28', '2019-10-09 11:26:28'),
(4, 'Taste', '2019-10-09 11:26:28', '2019-10-09 11:26:28');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `tax` double(5,2) DEFAULT 0.00,
  `delivery_fee` double(5,2) DEFAULT 0.00,
  `hint` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `driver_id` int(10) UNSIGNED DEFAULT NULL,
  `delivery_address_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Order Received', '2019-08-30 14:39:28', '2019-10-15 16:03:14'),
(2, 'Preparing', '2019-10-15 16:03:50', '2019-10-15 16:03:50'),
(3, 'Ready', '2019-10-15 16:04:30', '2019-10-15 16:04:30'),
(4, 'On the Way', '2019-10-15 16:04:13', '2019-10-15 16:04:13'),
(5, 'Delivered', '2019-10-15 16:04:30', '2019-10-15 16:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'users.profile', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(2, 'dashboard', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(3, 'medias.create', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(4, 'medias.delete', 'web', '2020-03-29 12:58:02', '2020-03-29 12:58:02', NULL),
(5, 'medias', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(6, 'permissions.index', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(7, 'permissions.edit', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(8, 'permissions.update', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(9, 'permissions.destroy', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(10, 'roles.index', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(11, 'roles.edit', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(12, 'roles.update', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(13, 'roles.destroy', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(14, 'customFields.index', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(15, 'customFields.create', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(16, 'customFields.store', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(17, 'customFields.show', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(18, 'customFields.edit', 'web', '2020-03-29 12:58:03', '2020-03-29 12:58:03', NULL),
(19, 'customFields.update', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(20, 'customFields.destroy', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(21, 'users.login-as-user', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(22, 'users.index', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(23, 'users.create', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(24, 'users.store', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(25, 'users.show', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(26, 'users.edit', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(27, 'users.update', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(28, 'users.destroy', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(29, 'app-settings', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(30, 'markets.index', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(31, 'markets.create', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(32, 'markets.store', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(33, 'markets.edit', 'web', '2020-03-29 12:58:04', '2020-03-29 12:58:04', NULL),
(34, 'markets.update', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(35, 'markets.destroy', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(36, 'categories.index', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(37, 'categories.create', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(38, 'categories.store', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(39, 'categories.edit', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(40, 'categories.update', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(41, 'categories.destroy', 'web', '2020-03-29 12:58:05', '2020-03-29 12:58:05', NULL),
(42, 'faqCategories.index', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(43, 'faqCategories.create', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(44, 'faqCategories.store', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(45, 'faqCategories.edit', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(46, 'faqCategories.update', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(47, 'faqCategories.destroy', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(48, 'orderStatuses.index', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(49, 'orderStatuses.show', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(50, 'orderStatuses.edit', 'web', '2020-03-29 12:58:06', '2020-03-29 12:58:06', NULL),
(51, 'orderStatuses.update', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(52, 'products.index', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(53, 'products.create', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(54, 'products.store', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(55, 'products.edit', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(56, 'products.update', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(57, 'products.destroy', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(58, 'galleries.index', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(59, 'galleries.create', 'web', '2020-03-29 12:58:07', '2020-03-29 12:58:07', NULL),
(60, 'galleries.store', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(61, 'galleries.edit', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(62, 'galleries.update', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(63, 'galleries.destroy', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(64, 'productReviews.index', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(65, 'productReviews.create', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(66, 'productReviews.store', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(67, 'productReviews.edit', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(68, 'productReviews.update', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(69, 'productReviews.destroy', 'web', '2020-03-29 12:58:08', '2020-03-29 12:58:08', NULL),
(76, 'options.index', 'web', '2020-03-29 12:58:09', '2020-03-29 12:58:09', NULL),
(77, 'options.create', 'web', '2020-03-29 12:58:09', '2020-03-29 12:58:09', NULL),
(78, 'options.store', 'web', '2020-03-29 12:58:09', '2020-03-29 12:58:09', NULL),
(79, 'options.show', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(80, 'options.edit', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(81, 'options.update', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(82, 'options.destroy', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(83, 'payments.index', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(84, 'payments.show', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(85, 'payments.update', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(86, 'faqs.index', 'web', '2020-03-29 12:58:10', '2020-03-29 12:58:10', NULL),
(87, 'faqs.create', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(88, 'faqs.store', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(89, 'faqs.edit', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(90, 'faqs.update', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(91, 'faqs.destroy', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(92, 'marketReviews.index', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(93, 'marketReviews.create', 'web', '2020-03-29 12:58:11', '2020-03-29 12:58:11', NULL),
(94, 'marketReviews.store', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(95, 'marketReviews.edit', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(96, 'marketReviews.update', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(97, 'marketReviews.destroy', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(98, 'favorites.index', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(99, 'favorites.create', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(100, 'favorites.store', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(101, 'favorites.edit', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(102, 'favorites.update', 'web', '2020-03-29 12:58:12', '2020-03-29 12:58:12', NULL),
(103, 'favorites.destroy', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(104, 'orders.index', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(105, 'orders.create', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(106, 'orders.store', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(107, 'orders.show', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(108, 'orders.edit', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(109, 'orders.update', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(110, 'orders.destroy', 'web', '2020-03-29 12:58:13', '2020-03-29 12:58:13', NULL),
(111, 'notifications.index', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(112, 'notifications.show', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(113, 'notifications.destroy', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(114, 'carts.index', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(115, 'carts.edit', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(116, 'carts.update', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(117, 'carts.destroy', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(118, 'currencies.index', 'web', '2020-03-29 12:58:14', '2020-03-29 12:58:14', NULL),
(119, 'currencies.create', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(120, 'currencies.store', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(121, 'currencies.edit', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(122, 'currencies.update', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(123, 'currencies.destroy', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(124, 'deliveryAddresses.index', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(125, 'deliveryAddresses.create', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(126, 'deliveryAddresses.store', 'web', '2020-03-29 12:58:15', '2020-03-29 12:58:15', NULL),
(127, 'deliveryAddresses.edit', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(128, 'deliveryAddresses.update', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(129, 'deliveryAddresses.destroy', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(130, 'drivers.index', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(131, 'drivers.create', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(132, 'drivers.store', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(133, 'drivers.show', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(134, 'drivers.edit', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(135, 'drivers.update', 'web', '2020-03-29 12:58:16', '2020-03-29 12:58:16', NULL),
(136, 'drivers.destroy', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(137, 'earnings.index', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(138, 'earnings.create', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(139, 'earnings.store', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(140, 'earnings.show', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(141, 'earnings.edit', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(142, 'earnings.update', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(143, 'earnings.destroy', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(144, 'driversPayouts.index', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(145, 'driversPayouts.create', 'web', '2020-03-29 12:58:17', '2020-03-29 12:58:17', NULL),
(146, 'driversPayouts.store', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(147, 'driversPayouts.show', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(148, 'driversPayouts.edit', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(149, 'driversPayouts.update', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(150, 'driversPayouts.destroy', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(151, 'marketsPayouts.index', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(152, 'marketsPayouts.create', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(153, 'marketsPayouts.store', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(154, 'marketsPayouts.show', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(155, 'marketsPayouts.edit', 'web', '2020-03-29 12:58:18', '2020-03-29 12:58:18', NULL),
(156, 'marketsPayouts.update', 'web', '2020-03-29 12:58:19', '2020-03-29 12:58:19', NULL),
(157, 'marketsPayouts.destroy', 'web', '2020-03-29 12:58:19', '2020-03-29 12:58:19', NULL),
(158, 'permissions.create', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(159, 'permissions.store', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(160, 'permissions.show', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(161, 'roles.create', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(162, 'roles.store', 'web', '2020-03-29 12:59:15', '2020-03-29 12:59:15', NULL),
(163, 'roles.show', 'web', '2020-03-29 12:59:16', '2020-03-29 12:59:16', NULL),
(164, 'fields.index', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(165, 'fields.create', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(166, 'fields.store', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(167, 'fields.edit', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(168, 'fields.update', 'web', '2020-04-11 13:04:39', '2020-04-11 13:04:39', NULL),
(169, 'fields.destroy', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(170, 'optionGroups.index', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(171, 'optionGroups.create', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(172, 'optionGroups.store', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(173, 'optionGroups.edit', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(174, 'optionGroups.update', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(175, 'optionGroups.destroy', 'web', '2020-04-11 13:04:40', '2020-04-11 13:04:40', NULL),
(176, 'requestedMarkets.index', 'web', '2020-08-13 12:58:02', '2020-08-13 12:58:02', NULL),
(183, 'coupons.index', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(184, 'coupons.create', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(185, 'coupons.store', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(186, 'coupons.edit', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(187, 'coupons.update', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(188, 'coupons.destroy', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(189, 'slides.index', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(190, 'slides.create', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(191, 'slides.store', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(192, 'slides.edit', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(193, 'slides.update', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL),
(194, 'slides.destroy', 'web', '2020-08-23 12:58:02', '2020-08-23 12:58:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `discount_price` double(8,2) DEFAULT 0.00,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `capacity` double(9,2) DEFAULT 0.00,
  `package_items_count` double(9,2) DEFAULT 0.00,
  `unit` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `featured` tinyint(1) DEFAULT 0,
  `deliverable` tinyint(1) DEFAULT 1,
  `market_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `discount_price`, `description`, `capacity`, `package_items_count`, `unit`, `featured`, `deliverable`, `market_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Salad', 11.01, 4.89, 'Dolor distinctio a totam qui. Mollitia pariatur qui accusamus nobis sed. Est quaerat aut sit magnam.', 276.81, 6.00, 'Kg', 0, 1, 4, 4, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(2, 'Onion', 28.73, 0.00, 'Nostrum tempore maiores officiis nulla est. Voluptatem illo dolorem deserunt vero sequi et ratione. Voluptatem autem velit et eligendi. Delectus dolores doloribus velit autem delectus.', 16.01, 4.00, 'm²', 0, 1, 6, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(3, 'Salt', 35.31, 32.68, 'Aliquam doloremque ea culpa omnis aut. Consectetur voluptatibus in sapiente animi blanditiis et quia. Cupiditate alias dolore neque veritatis odio. Tempore quas sunt aut dolorum sit.', 367.47, 1.00, 'm', 0, 0, 7, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(4, 'Bread', 31.75, 26.52, 'Expedita incidunt vitae vitae perferendis tempora in. Nemo magnam non et doloribus tenetur consequatur. Rerum aut et iste maxime cupiditate eos.', 8.61, 6.00, 'L', 1, 0, 2, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(5, 'Sandwich', 33.66, 29.91, 'Rerum excepturi quas exercitationem. In facere ut deleniti labore mollitia. Amet voluptatem cum dolor. Et facere et consequatur et ut aut ipsa.', 169.12, 4.00, 'ml', 0, 1, 10, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(6, 'Milk', 44.26, 0.00, 'Nesciunt veniam ut magni. Quis harum nemo excepturi adipisci inventore. Libero rerum cum voluptas.', 199.45, 2.00, 'm', 0, 1, 8, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(7, 'Salad', 21.65, 0.00, 'Modi velit et occaecati nihil adipisci tempore. Culpa nihil repellendus alias dolores. Dicta odio explicabo enim suscipit.', 445.21, 2.00, 'm²', 1, 0, 8, 2, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(8, 'Cheese', 26.06, 20.82, 'Ducimus non rerum et laborum dicta accusantium cupiditate et. Dignissimos quia aliquam reprehenderit. Et qui voluptates illum architecto.', 465.48, 1.00, 'g', 1, 1, 7, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(9, 'Eggs', 34.30, 0.00, 'Expedita error et voluptatem veritatis. Quibusdam eos quisquam quis fugit molestiae. Et eum alias accusamus est. Doloremque eos sit voluptatem dignissimos nihil.', 319.36, 1.00, 'L', 0, 0, 5, 4, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(10, 'Honey', 24.88, 0.00, 'Sed sed sit fuga suscipit voluptatibus ea et. Magni ex consequatur quae dolorem qui corporis. Ex laudantium excepturi hic quis asperiores nihil sapiente. Necessitatibus illo nisi eum ratione.', 90.26, 6.00, 'g', 1, 0, 1, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(11, 'Aspirin', 25.27, 21.36, 'Expedita nisi assumenda ullam omnis debitis ipsa. Facilis sapiente numquam sed unde. Minus qui minus ut odio et.', 337.08, 4.00, 'ml', 0, 0, 10, 2, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(12, 'Steak', 15.85, 7.13, 'Minus quasi temporibus aut atque aut aut. Ipsam nisi consequatur magnam vitae. Dicta corrupti vel perferendis.', 424.81, 4.00, 'g', 0, 1, 4, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(13, 'Spaghetti', 35.51, 33.82, 'Expedita sit non sint laudantium ipsum nisi sed. Assumenda omnis ullam porro amet. Tenetur sed fugit ut eos nesciunt incidunt quasi. Consequuntur ab repudiandae quia quod et.', 166.40, 6.00, 'Kg', 0, 0, 4, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(14, 'Soup', 21.39, 15.88, 'Dolor voluptatem aperiam perferendis aliquid. Architecto provident ut labore non. Quis voluptates occaecati iste alias. Expedita officia iste nobis voluptatem.', 178.26, 5.00, 'Kg', 0, 0, 4, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(15, 'Pasta', 26.89, 19.14, 'Ad cum rerum hic. In dolores et perspiciatis voluptate molestias. Amet perspiciatis consequuntur corporis dolores autem quisquam rerum.', 164.47, 5.00, 'm²', 0, 0, 10, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(16, 'Soup', 10.16, 0.00, 'Eum sit porro aut quisquam nobis velit exercitationem voluptatibus. Itaque eum nemo aspernatur numquam tenetur praesentium vitae. Vel eveniet earum officiis tempora.', 309.67, 6.00, 'Kg', 1, 0, 9, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(17, 'Milk', 22.49, 0.00, 'Assumenda ex dicta mollitia sit et. Laboriosam voluptas ratione inventore dicta voluptatum rerum rerum. Ullam dolor aut excepturi voluptates.', 251.87, 6.00, 'g', 1, 0, 9, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(18, 'Milk', 32.05, 0.00, 'Totam dignissimos et sit suscipit error quae quibusdam. Laboriosam odit quo adipisci rerum.', 202.02, 5.00, 'Oz', 0, 0, 6, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(19, 'Eggs', 26.71, 18.33, 'Distinctio provident doloremque voluptate quibusdam deserunt qui. Delectus sed est aut voluptatibus. Dolorem et nisi in at esse.', 153.60, 2.00, 'Kg', 1, 1, 2, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(20, 'Tuna steak', 18.80, 0.00, 'Sint et dolor et cum consequatur dolores. Sunt dolore dolore consequatur excepturi ut dolorum ea.', 464.30, 2.00, 'g', 1, 1, 9, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(21, 'Salad', 48.93, 0.00, 'Quos voluptas occaecati quo inventore dolor sed. Inventore ipsa officia quaerat dicta dolores. Nobis deleniti voluptatibus modi repellendus voluptatem accusantium.', 263.79, 6.00, 'Kg', 0, 0, 3, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(22, 'Salad', 43.35, 38.15, 'Earum aperiam vitae quibusdam aut tempore ipsum velit ad. Mollitia repellat aliquid maxime occaecati maiores modi fugiat. Maiores ad qui sit accusamus.', 32.42, 2.00, 'm²', 1, 1, 1, 2, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(23, 'Salt', 16.69, 0.00, 'Qui est culpa dolores et ullam. Ratione aut consequatur illo non. Fugiat expedita vel et autem quod quo ab fuga.', 112.71, 4.00, 'g', 0, 0, 10, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(24, 'Soup', 14.88, 7.53, 'Nihil qui magni maiores ratione. Sunt deserunt ea necessitatibus impedit.', 77.81, 4.00, 'm²', 1, 1, 8, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(25, 'Eggs', 32.75, 23.30, 'Aut magnam et molestias ut eos. Deleniti laboriosam aut quia repellendus vel ea et. Voluptatem sed eveniet ipsum velit et necessitatibus debitis. Voluptas id consequatur reiciendis.', 459.80, 5.00, 'L', 1, 1, 7, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(26, 'Soup', 39.80, 35.74, 'Aut velit labore voluptas et. Hic suscipit omnis ea voluptatem. Est sit aut quos recusandae. Cupiditate sed et sed illo quaerat.', 346.80, 2.00, 'm', 1, 0, 8, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(27, 'Tuna steak', 40.20, 36.26, 'Totam qui consequatur tenetur molestias quis. Similique et saepe et. Ut qui suscipit itaque nemo est possimus quas.', 11.32, 2.00, 'Kg', 0, 0, 2, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(28, 'Sandwich', 35.08, 0.00, 'Sint illo eaque explicabo cum officia. Corrupti aperiam qui sint incidunt aut ex error. In non quasi non facere autem autem. Veniam velit a unde non id. Nostrum voluptas nemo neque sit voluptas.', 350.10, 6.00, 'Kg', 0, 1, 3, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(29, 'Eggs', 47.09, 0.00, 'Quasi aliquid reiciendis autem et quasi. Ea sapiente reprehenderit impedit. Placeat quo quis ut tempore voluptatibus magni sed voluptatem.', 246.43, 4.00, 'Oz', 0, 1, 5, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(30, 'Cookie', 41.47, 0.00, 'Atque tempore ea laborum. Alias rerum unde aut molestiae doloremque non.', 331.87, 6.00, 'm', 0, 0, 5, 6, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(31, 'Tuna steak', 16.54, 0.00, 'Necessitatibus laudantium praesentium et quasi numquam cupiditate vel. Laborum in dolor libero mollitia fuga ut. Dolorum accusantium quisquam repellat.', 323.46, 5.00, 'Oz', 1, 1, 9, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(32, 'Aspirin', 12.64, 3.56, 'Illo laborum ipsa animi esse. Optio sed quis voluptatem provident ea. Exercitationem molestiae sapiente eaque nam illum. Facilis velit quibusdam veniam aut.', 278.24, 3.00, 'Oz', 0, 1, 4, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(33, 'Sandwich', 19.72, 0.00, 'Deserunt molestiae recusandae aut. Et corrupti occaecati et quos. Voluptatibus optio atque sapiente quae similique odio et. Nulla laudantium totam quae id porro vel delectus.', 223.56, 2.00, 'Kg', 1, 0, 7, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(34, 'Rice', 27.78, 25.63, 'Eos esse ipsam dolorem qui. Nam voluptatem porro et numquam. Dolorum voluptatem magni a ipsam nemo non.', 379.42, 1.00, 'Oz', 1, 1, 4, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(35, 'Cheese', 34.22, 32.02, 'Reprehenderit et culpa aut quia error in. Ea illum temporibus est. Vero et maxime et esse odio et tenetur et. Qui necessitatibus sed reiciendis fugit voluptatum.', 366.50, 1.00, 'Oz', 1, 0, 1, 3, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(36, 'Fish', 25.35, 18.81, 'Quas ut totam similique ea nesciunt. Ea aperiam aut dignissimos aut est dolores qui. Libero odio voluptates occaecati.', 283.70, 6.00, 'Oz', 0, 1, 2, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(37, 'Cookie', 36.84, 0.00, 'Aut repudiandae pariatur omnis dolores. Dolor in mollitia neque modi harum fugiat nam illo. Nihil et blanditiis asperiores labore error deserunt animi. Error eaque et quasi.', 104.99, 6.00, 'm²', 1, 1, 2, 4, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(38, 'Rice', 24.17, 0.00, 'Esse odit quam consectetur eveniet dicta quas est. Qui deleniti ex aut nostrum veritatis ut provident aut. Qui sunt id est non iste neque.', 395.18, 4.00, 'g', 0, 1, 7, 1, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(39, 'Pasta', 33.23, 31.15, 'Quos rerum rerum repellendus nam molestias neque blanditiis quas. Quod aut rerum reiciendis et deserunt numquam rerum. Illum autem neque quia. Possimus ipsam aut ut inventore praesentium aut.', 257.74, 4.00, 'Oz', 1, 0, 1, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(40, 'Acarbose', 44.68, 40.24, 'Sit ut et adipisci quod dolor ut aut. Sit saepe nemo sunt autem voluptas aut rerum.', 379.41, 3.00, 'g', 1, 0, 9, 5, '2022-01-04 10:36:16', '2022-01-04 10:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_order_options`
--

CREATE TABLE `product_order_options` (
  `product_order_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `review`, `rate`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'Some of the Mock Turtle said with some difficulty, as it spoke (it was Bill, I fancy--Who\'s to go through next walking about at the other paw, \'lives a March Hare. Alice was silent. The King laid.', 2, 1, 23, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(2, 'Alice, they all quarrel so dreadfully one can\'t hear oneself speak--and they don\'t give birthday presents like that!\' But she did not quite like the look of the window, and one foot to the confused.', 2, 6, 11, '2022-01-04 10:36:16', '2022-01-04 10:36:16'),
(3, 'I\'ll eat it,\' said the King. \'Nothing whatever,\' said Alice. \'Why, there they lay sprawling about, reminding her very much of it in with the other: the Duchess was sitting next to her. The Cat.', 1, 3, 24, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 'Queens, and among them Alice recognised the White Rabbit with pink eyes ran close by it, and yet it was quite out of breath, and said anxiously to herself, \'to be going messages for a minute or two.', 2, 4, 27, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 'Majesty must cross-examine THIS witness.\' \'Well, if I fell off the subjects on his flappers, \'--Mystery, ancient and modern, with Seaography: then Drawling--the Drawling-master was an uncomfortably.', 3, 6, 10, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(6, 'So they began running about in all my life, never!\' They had a wink of sleep these three weeks!\' \'I\'m very sorry you\'ve been annoyed,\' said Alice, \'and if it thought that she was now more than that.', 3, 6, 22, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(7, 'The next witness would be so proud as all that.\' \'With extras?\' asked the Mock Turtle. \'Very much indeed,\' said Alice. \'Nothing WHATEVER?\' persisted the King. The White Rabbit cried out, \'Silence in.', 2, 5, 5, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(8, 'I needn\'t be afraid of it. She went on \'And how did you begin?\' The Hatter was the Cat remarked. \'Don\'t be impertinent,\' said the King and the constant heavy sobbing of the song, \'I\'d have said to.', 5, 5, 15, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(9, 'Alice; \'you needn\'t be so proud as all that.\' \'Well, it\'s got no business of MINE.\' The Queen turned angrily away from him, and very nearly getting up and rubbed its eyes: then it chuckled. \'What.', 3, 4, 29, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(10, 'The pepper when he sneezes: He only does it to be beheaded!\' \'What for?\' said Alice. \'Call it what you mean,\' the March Hare, who had been to a lobster--\' (Alice began to repeat it, but her voice.', 3, 6, 23, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(11, 'Then it got down off the fire, and at last it sat for a conversation. Alice replied, rather shyly, \'I--I hardly know, sir, just at first, the two creatures got so much already, that it might tell.', 3, 5, 4, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(12, 'Dinah, and saying \"Come up again, dear!\" I shall have some fun now!\' thought Alice. \'I\'m glad they\'ve begun asking riddles.--I believe I can say.\' This was such a noise inside, no one to listen to.', 1, 5, 7, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(13, 'Do you think, at your age, it is I hate cats and dogs.\' It was opened by another footman in livery came running out of sight before the end of the bread-and-butter. Just at this corner--No, tie \'em.', 4, 3, 23, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(14, 'For some minutes the whole she thought it over a little bottle that stood near. The three soldiers wandered about in the air: it puzzled her a good thing!\' she said this, she was not a VERY.', 2, 3, 29, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(15, 'I wonder?\' As she said to one of the sense, and the executioner myself,\' said the Gryphon, and the words all coming different, and then keep tight hold of this remark, and thought to herself, \'after.', 2, 1, 26, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(16, 'Alice laughed so much frightened that she was talking. Alice could see her after the rest were quite silent, and looked at them with large eyes like a star-fish,\' thought Alice. \'I\'m glad they\'ve.', 4, 2, 11, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(17, 'Alice, that she still held the pieces of mushroom in her haste, she had this fit) An obstacle that came between Him, and ourselves, and it. Don\'t let me hear the words:-- \'I speak severely to my.', 2, 2, 7, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(18, 'Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, but in a melancholy way, being quite unable to move. She soon got it out again, and put it in time,\' said the Hatter.', 1, 2, 23, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(19, 'I\'ve had such a curious dream, dear, certainly: but now run in to your tea; it\'s getting late.\' So Alice began to repeat it, but her head pressing against the ceiling, and had to leave it behind?\'.', 5, 5, 21, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(20, 'Alice, as she could not help bursting out laughing: and when she turned to the tarts on the floor, and a large dish of tarts upon it: they looked so good, that it made no mark; but he now hastily.', 5, 2, 20, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(21, 'But here, to Alice\'s side as she could do to ask: perhaps I shall never get to the Gryphon. \'Then, you know,\' said Alice in a sulky tone; \'Seven jogged my elbow.\' On which Seven looked up and throw.', 4, 4, 26, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(22, 'Why, there\'s hardly enough of me left to make SOME change in my size; and as he spoke. \'A cat may look at all the other ladder?--Why, I hadn\'t to bring but one; Bill\'s got to do,\' said the Hatter.', 5, 1, 8, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(23, 'THAT\'S a good opportunity for repeating his remark, with variations. \'I shall sit here,\' he said, turning to Alice, and tried to look for her, and said, very gravely, \'I think, you ought to eat some.', 5, 2, 9, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(24, 'Dinah, tell me the list of singers. \'You may not have lived much under the window, I only wish people knew that: then they wouldn\'t be so easily offended!\' \'You\'ll get used up.\' \'But what did the.', 4, 4, 29, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(25, 'It looked good-natured, she thought: still it was only sobbing,\' she thought, and looked at Alice. \'I\'M not a regular rule: you invented it just now.\' \'It\'s the oldest rule in the distance, and she.', 5, 2, 9, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(26, 'Alice thought she might as well say that \"I see what I eat\" is the reason so many out-of-the-way things had happened lately, that Alice could not taste theirs, and the White Rabbit: it was done.', 2, 3, 3, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(27, 'Queen, turning purple. \'I won\'t!\' said Alice. \'I mean what I say--that\'s the same when I grow at a king,\' said Alice. \'Come, let\'s try Geography. London is the driest thing I ask! It\'s always six.', 3, 4, 23, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(28, 'Sing her \"Turtle Soup,\" will you, won\'t you join the dance?\"\' \'Thank you, sir, for your interesting story,\' but she added, \'and the moral of that is--\"Oh, \'tis love, that makes people hot-tempered,\'.', 4, 2, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(29, 'Alice ventured to remark. \'Tut, tut, child!\' said the Gryphon: \'I went to the Gryphon. \'The reason is,\' said the Duchess: \'what a clear way you have to ask help of any one; so, when the White.', 4, 6, 13, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(30, 'I only knew how to get her head down to them, and then sat upon it.) \'I\'m glad they don\'t seem to come out among the distant sobs of the song, \'I\'d have said to herself how she was ready to ask them.', 3, 5, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `default` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'superadmin', 'web', 0, '2018-07-21 14:37:56', '2019-09-07 20:42:01', NULL),
(3, 'admin', 'web', 0, '2019-09-07 20:41:38', '2019-09-07 20:41:38', NULL),
(4, 'market', 'web', 1, '2019-09-07 20:41:54', '2019-09-07 20:41:54', NULL),
(5, 'branches', 'web', 0, '2019-12-15 16:50:21', '2019-12-15 16:50:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(5, 2),
(5, 3),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(27, 3),
(27, 4),
(27, 5),
(28, 2),
(29, 2),
(30, 2),
(30, 3),
(30, 4),
(30, 5),
(31, 2),
(31, 3),
(31, 4),
(32, 2),
(32, 3),
(32, 4),
(33, 2),
(33, 3),
(34, 2),
(34, 3),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(42, 3),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(48, 3),
(48, 5),
(50, 2),
(51, 2),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(53, 2),
(53, 3),
(54, 2),
(54, 3),
(55, 2),
(55, 3),
(56, 2),
(56, 3),
(57, 2),
(57, 3),
(58, 2),
(58, 3),
(59, 2),
(59, 3),
(60, 2),
(60, 3),
(61, 2),
(61, 3),
(62, 2),
(62, 3),
(63, 2),
(63, 3),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(69, 2),
(76, 2),
(76, 3),
(77, 2),
(77, 3),
(78, 2),
(78, 3),
(80, 2),
(80, 3),
(81, 2),
(81, 3),
(82, 2),
(82, 3),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(84, 2),
(85, 2),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(92, 3),
(92, 4),
(92, 5),
(95, 2),
(95, 3),
(95, 4),
(95, 5),
(96, 2),
(96, 3),
(96, 4),
(96, 5),
(97, 2),
(98, 2),
(98, 3),
(98, 4),
(98, 5),
(103, 2),
(103, 3),
(103, 4),
(103, 5),
(104, 2),
(104, 3),
(104, 4),
(104, 5),
(107, 2),
(107, 3),
(107, 4),
(107, 5),
(108, 2),
(108, 3),
(109, 2),
(109, 3),
(110, 2),
(110, 3),
(111, 2),
(111, 3),
(111, 4),
(111, 5),
(112, 2),
(113, 2),
(113, 3),
(113, 4),
(113, 5),
(114, 2),
(114, 3),
(114, 4),
(114, 5),
(117, 2),
(117, 3),
(117, 4),
(117, 5),
(118, 2),
(119, 2),
(120, 2),
(121, 2),
(122, 2),
(123, 2),
(124, 2),
(127, 2),
(128, 2),
(129, 2),
(130, 2),
(130, 3),
(130, 5),
(131, 2),
(134, 2),
(134, 3),
(135, 2),
(135, 3),
(137, 2),
(137, 3),
(138, 2),
(144, 2),
(144, 5),
(145, 2),
(145, 3),
(145, 5),
(146, 2),
(146, 3),
(146, 5),
(148, 2),
(149, 2),
(151, 2),
(151, 3),
(152, 2),
(152, 3),
(153, 2),
(153, 3),
(155, 2),
(156, 2),
(158, 2),
(159, 2),
(160, 2),
(161, 2),
(162, 2),
(163, 2),
(164, 2),
(164, 3),
(164, 4),
(164, 5),
(165, 2),
(166, 2),
(167, 2),
(168, 2),
(169, 2),
(170, 2),
(170, 3),
(171, 2),
(171, 3),
(172, 2),
(172, 3),
(173, 2),
(174, 2),
(175, 2),
(176, 2),
(176, 3),
(176, 4),
(176, 5),
(183, 2),
(183, 3),
(183, 4),
(183, 5),
(184, 2),
(185, 2),
(186, 2),
(186, 3),
(187, 2),
(187, 3),
(188, 2),
(189, 2),
(190, 2),
(191, 2),
(192, 2),
(193, 2),
(194, 2);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED DEFAULT 0,
  `text` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'start',
  `text_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `indicator_color` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_fit` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'cover',
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `market_id` int(10) UNSIGNED DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `order`, `text`, `button`, `text_position`, `text_color`, `button_color`, `background_color`, `indicator_color`, `image_fit`, `product_id`, `market_id`, `enabled`, `created_at`, `updated_at`) VALUES
(1, 1, 'Harum est eveniet ut.', 'Get Discount', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', 12, NULL, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(2, 1, 'Tenetur corporis eaque totam voluptatum.', 'Discover It', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', 8, NULL, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(3, 5, 'Fugiat sit quod tempore doloremque.', 'Discover It', 'end', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', 3, NULL, 0, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(4, 1, 'Aliquam quo perferendis dicta.', 'Discover It', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', 7, NULL, 1, '2022-01-04 10:36:17', '2022-01-04 10:36:17'),
(5, 5, 'Fugiat quos facilis sapiente fuga.', 'Get Discount', 'start', '#25d366', '#25d366', '#ccccdd', '#25d366', 'cover', NULL, 4, 0, '2022-01-04 10:36:17', '2022-01-04 10:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `braintree_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `api_token`, `device_token`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `braintree_id`, `paypal_email`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Michael E. Quinn', 'admin@demo.com', '$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu', 'PivvPlsQWxPl1bB5KrbKNBuraJit0PrUZekQUgtLyTRuyBq921atFtoR1HuA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'T4PQhFvBcAA7k02f7ejq4I7z7QKKnvxQLV0oqGnuS6Ktz6FdWULrWrzZ3oYn', '2018-08-06 20:58:41', '2019-09-27 05:49:45'),
(2, 'Barbara J. Glanz', 'manager@demo.com', '$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu', 'tVSfIKRSX2Yn8iAMoUS3HPls84ycS8NAxO2dj2HvePbbr4WHorp4gIFRmFwB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5nysjzVKI4LU92bjRqMUSYdOaIo1EcPC3pIMb6Tcj2KXSUMriGrIQ1iwRdd0', '2018-08-14 15:06:28', '2019-09-25 20:09:35'),
(3, 'Charles W. Abeyta', 'client@demo.com', '$2y$10$EBubVy3wDbqNbHvMQwkj3OTYVitL8QnHvh/zV0ICVOaSbALy5dD0K', 'fXLu7VeYgXDu82SkMxlLPG1mCAXc4EBIx6O5isgYVIKFQiHah0xiOHmzNsBv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'V6PIUfd8JdHT2zkraTlnBcRSINZNjz5Ou7N0WtUGRyaTweoaXKpSfij6UhqC', '2019-10-12 20:31:26', '2020-03-29 15:44:30'),
(4, 'Robert E. Brock', 'client1@demo.com', '$2y$10$pmdnepS1FhZUMqOaFIFnNO0spltJpziz3j13UqyEwShmLhokmuoei', 'Czrsk9rwD0c75NUPkzNXM2WvbxYHKj8p0nG29pjKT0PZaTgMVzuVyv4hOlte', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-15 15:55:39', '2020-03-29 15:59:39'),
(5, 'Sanchez Roberto', 'driver@demo.com', '$2y$10$T/jwzYDJfC8c9CdD5PbpuOKvEXlpv4.RR1jMT0PgIMT.fzeGw67JO', 'OuMsmU903WMcMhzAbuSFtxBekZVdXz66afifRo3YRCINi38jkXJ8rpN0FcfS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-15 16:49:44', '2020-03-29 15:22:10'),
(6, 'John Doe', 'driver1@demo.com', '$2y$10$YF0jCx2WCQtfZOq99hR8kuXsAE0KSnu5OYSomRtI9iCVguXDoDqVm', 'zh9mzfNO2iPtIxj6k4Jpj8flaDyOsxmlGRVUZRnJqOGBr8IuDyhb3cGoncvS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-29 15:28:04', '2020-03-29 15:28:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_markets`
--

CREATE TABLE `user_markets` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `market_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_markets`
--

INSERT INTO `user_markets` (`user_id`, `market_id`) VALUES
(1, 2),
(1, 3),
(1, 5),
(1, 6),
(2, 3),
(2, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_settings_key_index` (`key`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `cart_options`
--
ALTER TABLE `cart_options`
  ADD PRIMARY KEY (`option_id`,`cart_id`),
  ADD KEY `cart_options_cart_id_foreign` (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_field_values_custom_field_id_foreign` (`custom_field_id`);

--
-- Indexes for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `delivery_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `discountables`
--
ALTER TABLE `discountables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discountables_coupon_id_foreign` (`coupon_id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_user_id_foreign` (`user_id`);

--
-- Indexes for table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drivers_payouts_user_id_foreign` (`user_id`);

--
-- Indexes for table `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `driver_markets_market_id_foreign` (`market_id`);

--
-- Indexes for table `earnings`
--
ALTER TABLE `earnings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `earnings_market_id_foreign` (`market_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faqs_faq_category_id_foreign` (`faq_category_id`);

--
-- Indexes for table `faq_categories`
--
ALTER TABLE `faq_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_product_id_foreign` (`product_id`),
  ADD KEY `favorites_user_id_foreign` (`user_id`);

--
-- Indexes for table `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD PRIMARY KEY (`option_id`,`favorite_id`),
  ADD KEY `favorite_options_favorite_id_foreign` (`favorite_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_market_id_foreign` (`market_id`);

--
-- Indexes for table `markets`
--
ALTER TABLE `markets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `markets_payouts_market_id_foreign` (`market_id`);

--
-- Indexes for table `market_fields`
--
ALTER TABLE `market_fields`
  ADD PRIMARY KEY (`field_id`,`market_id`),
  ADD KEY `market_fields_market_id_foreign` (`market_id`);

--
-- Indexes for table `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `market_reviews_user_id_foreign` (`user_id`),
  ADD KEY `market_reviews_market_id_foreign` (`market_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_product_id_foreign` (`product_id`),
  ADD KEY `options_option_group_id_foreign` (`option_group_id`);

--
-- Indexes for table `option_groups`
--
ALTER TABLE `option_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_order_status_id_foreign` (`order_status_id`),
  ADD KEY `orders_driver_id_foreign` (`driver_id`),
  ADD KEY `orders_delivery_address_id_foreign` (`delivery_address_id`),
  ADD KEY `orders_payment_id_foreign` (`payment_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_market_id_foreign` (`market_id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_orders_product_id_foreign` (`product_id`),
  ADD KEY `product_orders_order_id_foreign` (`order_id`);

--
-- Indexes for table `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD PRIMARY KEY (`product_order_id`,`option_id`),
  ADD KEY `product_order_options_option_id_foreign` (`option_id`);

--
-- Indexes for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_user_id_foreign` (`user_id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slides_product_id_foreign` (`product_id`),
  ADD KEY `slides_market_id_foreign` (`market_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `user_markets`
--
ALTER TABLE `user_markets`
  ADD PRIMARY KEY (`user_id`,`market_id`),
  ADD KEY `user_markets_market_id_foreign` (`market_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `discountables`
--
ALTER TABLE `discountables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `earnings`
--
ALTER TABLE `earnings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `faq_categories`
--
ALTER TABLE `faq_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `markets`
--
ALTER TABLE `markets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `market_reviews`
--
ALTER TABLE `market_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `option_groups`
--
ALTER TABLE `option_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cart_options`
--
ALTER TABLE `cart_options`
  ADD CONSTRAINT `cart_options_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cart_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `custom_field_values`
--
ALTER TABLE `custom_field_values`
  ADD CONSTRAINT `custom_field_values_custom_field_id_foreign` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD CONSTRAINT `delivery_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discountables`
--
ALTER TABLE `discountables`
  ADD CONSTRAINT `discountables_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `drivers`
--
ALTER TABLE `drivers`
  ADD CONSTRAINT `drivers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `drivers_payouts`
--
ALTER TABLE `drivers_payouts`
  ADD CONSTRAINT `drivers_payouts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `driver_markets`
--
ALTER TABLE `driver_markets`
  ADD CONSTRAINT `driver_markets_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `driver_markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `earnings`
--
ALTER TABLE `earnings`
  ADD CONSTRAINT `earnings_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faqs`
--
ALTER TABLE `faqs`
  ADD CONSTRAINT `faqs_faq_category_id_foreign` FOREIGN KEY (`faq_category_id`) REFERENCES `faq_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorite_options`
--
ALTER TABLE `favorite_options`
  ADD CONSTRAINT `favorite_options_favorite_id_foreign` FOREIGN KEY (`favorite_id`) REFERENCES `favorites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorite_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `markets_payouts`
--
ALTER TABLE `markets_payouts`
  ADD CONSTRAINT `markets_payouts_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `market_fields`
--
ALTER TABLE `market_fields`
  ADD CONSTRAINT `market_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_fields_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `market_reviews`
--
ALTER TABLE `market_reviews`
  ADD CONSTRAINT `market_reviews_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `market_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_option_group_id_foreign` FOREIGN KEY (`option_group_id`) REFERENCES `option_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_delivery_address_id_foreign` FOREIGN KEY (`delivery_address_id`) REFERENCES `delivery_addresses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `product_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_orders_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_order_options`
--
ALTER TABLE `product_order_options`
  ADD CONSTRAINT `product_order_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_order_options_product_order_id_foreign` FOREIGN KEY (`product_order_id`) REFERENCES `product_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `slides_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `user_markets`
--
ALTER TABLE `user_markets`
  ADD CONSTRAINT `user_markets_market_id_foreign` FOREIGN KEY (`market_id`) REFERENCES `markets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_markets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

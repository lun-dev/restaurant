<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverFrontLicenseTableDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->date('birth_of_date')->nullable();
            $table->string('driver_front_license')->nullable();
            $table->string('driver_back_license')->nullable();
            $table->string('ID_front')->nullable();
            $table->string('ID_back')->nullable();
            $table->string('Vehicle_front_license')->nullable();
            $table->string('Vehicle_back_license')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            //
        });
    }
}

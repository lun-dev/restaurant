<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMarketDaysTable extends Migration {

	public function up()
	{
		Schema::create('market_days', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('market_id')->unsigned()->nullable();
			$table->integer('day_id')->unsigned();
			$table->string('start_from')->nullable();
			$table->string('cloase_at')->nullable();
            $table->foreign('market_id')->references('id')->on('markets')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('day_id')->references('id')->on('days')
                ->onDelete('cascade')
                ->onUpdate('cascade');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('market_days');
	}
}
